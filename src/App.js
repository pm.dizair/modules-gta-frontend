import Router from "@/router";
import { GoogleOAuthProvider } from "@react-oauth/google";
import { SnackbarProvider } from "notistack";
import { Provider } from "react-redux";
import "@/components/modals/register";
import store from "./store";
import SuccessSnacks from "./layout/snacks/success";
import InfoSnacks from "./layout/snacks/info";
import CookiesSnacks from "./layout/snacks/cookies";
import LowBalanceSnacks from "./layout/snacks/lowbalance";
import { register } from "swiper/element/bundle";
import ErrorSnacks from "./layout/snacks/error";
import ReactGA4 from "react-ga4";
import { TawkMessengerProvider } from "./components/contexts/tawk";
import PaymentTabMiddleware from "./modules/empty-pages/payment-tab-middleware";

if (window.location.href.includes(process.env.REACT_APP_DOMAIN_URL)) {
  ReactGA4.initialize(process.env.REACT_APP_GOOGLE_ANALYTIC_TRACKING_ID);
} else {
  ReactGA4.initialize(process.env.REACT_APP_GOOGLE_ANALYTIC_TRACKING_ID_CO);
}
register();

const App = () => (
  <Provider store={store}>
    <PaymentTabMiddleware>
      <TawkMessengerProvider>
        <GoogleOAuthProvider clientId={process.env.REACT_APP_GOOGLE_CLIENT_ID}>
          <SnackbarProvider
            maxSnack={3}
            Components={{
              success: SuccessSnacks,
              error: ErrorSnacks,
              info: InfoSnacks,
              lowBalance: LowBalanceSnacks,
              cookies: CookiesSnacks,
            }}
            autoHideDuration={5000}
            anchorOrigin={{
              vertical: "top",
              horizontal: "center",
            }}
            preventDuplicate={true}
          >
            <Router />
          </SnackbarProvider>
        </GoogleOAuthProvider>
      </TawkMessengerProvider>
    </PaymentTabMiddleware>
  </Provider>
);

export default App;
