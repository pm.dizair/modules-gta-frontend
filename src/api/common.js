import { baseApi } from "./$base";

export const commonApi = baseApi.injectEndpoints({
  endpoints: (build) => ({
    siteText: build.query({
      query: (language) => ({ url: `${language}/site-text/`, method: "GET" }),
    }),
    getSocials: build.query({
      query: () => ({ url: "content/socials/", method: "GET" }),
    }),
    getCities: build.query({
      query: (id) => ({ url: `geo/cities/${id}/`, method: "GET" }),
    }),
    getBlackListCities: build.query({
      query: (id) => ({ url: `geo/blacklist/cities/`, method: "GET" }),
    }),
    getBlackListCountries: build.query({
      query: (id) => ({ url: `geo/blacklist/countries/`, method: "GET" }),
    }),
    getLanguages: build.query({
      query: (id) => ({ url: `languages/`, method: "GET" }),
    }),
    getCommon: build.query({
      query: (id) => ({ url: `common/`, method: "GET" }),
    }),
  }),
});

export const {
  useSiteTextQuery,
  useGetCommonQuery,
  useGetCitiesQuery,
  useGetSocialsQuery,
  useGetLanguagesQuery,
  useGetBlackListCitiesQuery,
  useGetBlackListCountriesQuery,
} = commonApi;
