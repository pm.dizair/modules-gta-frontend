import { cacheContentAction } from "@/store/actions/cache";
import { baseApi } from "./$base";
import { getCachedQueryFn } from "./helpers/getCachedQueryFn";

export const contentApi = baseApi.injectEndpoints({
  overrideExisting: false,
  endpoints: (build) => ({
    getFooter: build.query({
      queryFn: getCachedQueryFn(
        "content",
        {
          url: "/footer/",
          method: "GET",
        },
        cacheContentAction
      ),
    }),
    getNavigation: build.query({
      queryFn: (body, ...args) => {
        const language = body?.language || localStorage.getItem("language");
        const fetch = getCachedQueryFn(
          "content",
          {
            url: `${language}/content/navigation/`,
            method: "GET",
          },
          cacheContentAction
        );

        return fetch(body, ...args);
      },
    }),
    getQuestion: build.query({
      queryFn: (body, ...args) => {
        const language = body?.language || localStorage.getItem("language");
        const fetch = getCachedQueryFn(
          "content",
          {
            url: `${language}/content/question/`,
            method: "GET",
          },
          cacheContentAction
        );
        return fetch(body, ...args);
      },
    }),
  }),
});

export const { useGetFooterQuery, useGetQuestionQuery, useGetNavigationQuery } =
  contentApi;
