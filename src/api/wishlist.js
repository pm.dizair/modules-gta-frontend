import { baseApi } from "./$base";

export const wishlistApi = baseApi.injectEndpoints({
  overrideExisting: false,
  endpoints: (build) => ({
    addOrRemoveWishlistGame: build.mutation({
      query: (id) => ({
        url: `wishlist/add-or-remove/${id}/`,
        method: "PUT",
      }),
    }),
  }),
});

export const { useAddOrRemoveWishlistGameMutation } = wishlistApi;
