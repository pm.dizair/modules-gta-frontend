export const overflowText = `
  text-overflow: ellipsis;
  overflow: hidden;
  width: 100%;
  white-space: nowrap;
`;
export const image = `
  width: 100%;
  height: 100%;
  display: block;
  object-fit: cover;
`;
export const GTA_TEXT = `
  font-family: "Saira Stencil One";
  color: #fff;
  font-weight: 400;
`;
export const modal_color = `
background: var(--blue);
border: 1px solid var(--grey);
`;
export const gradient = {
  RED_WHITE:
    "background: linear-gradient(148.28deg, #F86BA8 19.1%, #FBACC1 52.95%, #FDE6D7 86.8%);",
  BLUE_WHITE:
    "background: linear-gradient(148.28deg, #6BC5F8 19.1%, #FDF0D7 86.8%);",
  VIOLET_BLUE:
    "background: linear-gradient(148.28deg, #B6A8F8 19.1%, #A2F4FF 86.8%);",
  GREEN_RED:
    "background: linear-gradient(148.28deg, #A1EA75 19.1%, #FB8181 86.8%);",
  GREEN_STRAWBERRY:
    "background: linear-gradient(148.28deg, #52DFE8 19.1%, #FF9FCB 56.83%, #FF8080 86.8%);",
  YELLOW_ORANGE:
    "background: linear-gradient(148.28deg, #F7D75A 19.1%, #F36F4A 86.8%);",
  PINK_GOLD:
    "background: linear-gradient(148.28deg, #DC76C6 19.1%, #FDFF93 86.8%);",
  RED_GREEN:
    "background: linear-gradient(148.28deg, #FF936A 19.1%, #DCD8A7 56.83%, #A8FAFF 86.8%);",
  GREEN_PINK:
    "background: linear-gradient(148.28deg, #78FAD3 19.1%, #F587C5 86.8%);",
};

export const flex_center = `
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const scroll_set = `
  &::-webkit-scrollbar {
    width: 3px;
    background: var(--gold_neon_hover);
    height: 0;
  }
  &::-webkit-scrollbar-thumb {
    background: var(--gold);
    border-radius: 2px;
  }
`;

export const getLineClamp = (lines = 4) =>
  `
    display: -webkit-box;
    -webkit-line-clamp: ${lines};
    -webkit-box-orient: vertical;
    overflow: hidden;
  `;
