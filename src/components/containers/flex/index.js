import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-start;
`;

const Flex = ({ children, p, m, width, alignItems, justifyContent }) => (
  <Wrapper
    style={{
      width: `${width}`,
      alignItems,
      justifyContent,
      padding: `${p}`,
      margin: `${m}`,
    }}
  >
    {children}
  </Wrapper>
);

export default Flex;
