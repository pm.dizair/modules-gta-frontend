import React from "react";
import styled from "styled-components";
import ShopBg from "@/assets/new/images/backgrounds/shop.webp";
import ProfileBg from "@/assets/new/images/backgrounds/profile.webp";
import DepositBg from "@/assets/new/images/backgrounds/deposit.webp";
import TournamentBg from "@/assets/new/images/backgrounds/tournament.webp";
import PAGES from "@/const/pages";

export const Wrapper = styled.section`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  background-image: ${(props) => `url(${props.bg})`};
  background-attachment: fixed;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
`;

const Parallax = ({ page, children }) => {
  const { PROFILE, DEPOSIT, TOURNAMENT, SHOP } = PAGES;
  const getImage = () => {
    switch (page) {
      case PROFILE:
        return ProfileBg;
      case DEPOSIT:
        return DepositBg;
      case TOURNAMENT:
        return TournamentBg;
      case SHOP:
        return ShopBg;
      default:
        break;
    }
  };
  return <Wrapper bg={getImage()}>{children}</Wrapper>;
};

export default Parallax;
