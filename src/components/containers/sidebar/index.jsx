import { useCommon } from "@/store/selectors";
import React, { useEffect, useRef } from "react";
import { useNavigate, useParams } from "react-router";
import { Load } from "./load";
import { Button, Wrapper } from "./style";
import IconPatron from "@/assets/icons/menu/patron.webp";
import { useMediaQuery } from "react-responsive";

export const SideBar = ({ sidebarData, load, roat, tabs = null }) => {
  const { language } = useCommon();
  const navigate = useNavigate();
  const tabRef = useRef(null);
  const { tab } = useParams();
  const isTablet = useMediaQuery({ maxWidth: 1280 });

  const redirectTo = (cur) => {
    window.scrollTo(0, 0);
    if (typeof tabs === "object" && Object.values(tabs).length) {
      navigate(`/${language}/${roat}/${tabs[cur.slug]}`);
    } else {
      navigate(`/${language}/${roat}/${cur.keyword}`);
    }
  };

  useEffect(() => {
    tabRef?.current?.scrollIntoView({
      behavior: "smooth",
      block: "nearest",
      inline: "center",
    });
  }, [tab]);

  if (load) {
    return <Load sidebarData={[1, 2, 3, 4, 5]} />;
  }

  return (
    <Wrapper className="wrapper">
      {sidebarData.map((cur, id) => (
        <Button
          title={cur.name}
          ref={tab === cur?.keyword ? tabRef : null}
          className="anim-gradient-outline-violent"
          isActive={
            tab ? cur?.keyword.includes(tab) : cur.value === "Personal Info"
          }
          key={id}
          onClick={(e) => {
            redirectTo(cur);
          }}
        >
          <span>{cur.name}</span>
          {!isTablet && <img src={IconPatron} alt="" />}
        </Button>
      ))}
    </Wrapper>
  );
};
