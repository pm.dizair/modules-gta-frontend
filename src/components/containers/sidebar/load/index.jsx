import React from "react";
import { SkeletonButton, Wrapper } from "../style";

export const Load = ({ sidebarData }) => {
  return (
    <Wrapper className="wrapper">
      {sidebarData.map((cur, id) => (
        <SkeletonButton key={id} className="skeleton-box-neon" />
      ))}
    </Wrapper>
  );
};
