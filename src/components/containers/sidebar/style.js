import { overflowText } from "@/assets/css/global";
import styled from "styled-components";
export const Wrapper = styled.aside`
  width: 20%;
  display: flex;
  align-items: flex-start;
  justify-content: flex-start;
  flex-direction: column;
  gap: 10px;
  ::-webkit-scrollbar {
    height: 0;
  }
  @media screen and (max-width: 1280px) {
    flex-direction: row;
    width: 100%;
    gap: 10px;
  }
  @media screen and (max-width: 1024px) {
    overflow-x: auto;
  }
  @media screen and (max-width: 767px) {
    padding: 0 16px;
  }
  @media screen and (min-width: 1281px) {
    position: sticky;
    top: 100px;
  }
`;
export const Button = styled.button`
  width: 100%;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  padding: 16px 40px 16px 40px;
  gap: 8px;
  font-size: 15px;
  border-radius: 6px;
  border: 1px solid transparent;
  border-radius: 6px;
  img {
    width: 24px;
  }
  span {
    width: calc(100% - 24px);
    ${overflowText}
    text-align: left;
  }
  svg {
    min-width: 24px;
    min-height: 24px;
  }
  @media screen and (max-width: 1600px) {
    padding: 10px 16px;
    font-size: 14px;
  }
  @media screen and (max-width: 1024px) {
    white-space: nowrap;
  }
  ${(props) =>
    props.isActive
      ? `
        color: #fff;
        background: var(--blue);
        
        border: 1px solid var(--gold_border);
          svg {
            path{
              stroke: #A3A6B6;
              fill: #fff;            
            }
          }

      `
      : `
        img {
          filter: grayscale(1)
        }
        background: var(--grey_dark); 
        border: 1px solid var(--grey_border);
          svg {
            path{
              stroke: #A3A6B6;
              fill: #fff;            
            }
          }
      `}
  @media screen and (max-width: 767px) {
    margin-bottom: 10px;
  }
`;
export const SkeletonButton = styled.div`
  height: 46px;
  width: 100%;
  min-width: 180px;
  padding: 16px 0 16px 16px;
  gap: 6px;
  font-size: 15px;
  border-radius: 12px;
  border-radius: 8px;
  transition: all 0.2s linear;
  span {
    width: calc(100% - 24px);
    text-align: left;
  }

  @media screen and (max-width: 1440px) {
    padding: 16px 12px;
    font-size: 12px;
  }
  @media screen and (max-width: 1280px) {
    font-size: 14px;
    padding: 12px;
  }
  @media screen and (max-width: 1024px) {
    white-space: nowrap;
    padding: 16px 12px;
  }
`;
