import React from "react";
import { Wrapper, Aspect, Content, Banner, InfoPanel, Title } from "./style";
import NeonSkeleton from "@/components/skeleton/neon";
import "react-loading-skeleton/dist/skeleton.css";

const CardLoader = () => {
  return (
    <Wrapper>
      <Aspect className="card_game-aspect">
        <Content>
          <Banner>
            <NeonSkeleton height={"100%"} />
          </Banner>
        </Content>
      </Aspect>
      <InfoPanel>
        <Title>
          <NeonSkeleton />
        </Title>
      </InfoPanel>
    </Wrapper>
  );
};
export default CardLoader;
