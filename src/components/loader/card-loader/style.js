import styled from 'styled-components';

export const Wrapper = styled.div`
  width: 100%;
  display: flex;
  position: relative;
  flex-direction: column;
  transition: all 0.3s linear;
  align-self: stretch;
  overflow: hidden;
  border-radius: 6px;
`;

export const Aspect = styled.div`
  position: relative;
  height: 0;
  border: none;
  padding-top: 75%;
`;
export const Content = styled.div`
  position: absolute;
  inset: 0;
  z-index: 3;
`;
export const Banner = styled.div`
  position: relative;
  width: 100%;
  height: 100%;
  overflow: hidden;
  border-radius: 6px;
  transition: 0.4s;
`;

export const Title = styled.h2`
  font-weight: 500;
  font-size: 14px;
  line-height: 18px;
  color: transparent !important;
  border-radius: 4px;
  width: 100%;
`;
export const InfoPanel = styled.div`
  width: 100%;
  position: relative;
  z-index: 3;
  padding: 5px 0;
  text-align: center;
`;
