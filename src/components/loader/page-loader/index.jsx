import { Wrapper, Logo } from "./style";
const PageLoader = () => {
  return (
    <Wrapper>
      <Logo src="https://file/logo.webp" alt="" />
    </Wrapper>
  );
};

export default PageLoader;
