import styled from "styled-components";

export const Background = styled.div`
  background: var(--black);
  height: 100vh;
  align-items: center;
`;

export const Wrapper = styled.div`
  width: 100%;
  height: 100%;
`;
export const Logo = styled.img`
  width: 200px;
  height: 94px;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translateX(-50%) translateY(-50%);
`;
