import React from "react";
import { Container, Gradient, Tree, Lines } from "./style";
export const PalmLoader = () => {
  return (
    <Container>
      <Gradient />
      <Tree />
      <Lines />
    </Container>
  );
};
