import styled, { keyframes } from "styled-components";
import palm from "@/assets/new/palm.png";
const lines = keyframes` 
  to {
    background-position: 0;
  }
  from {
    background-position: 0 400px;
  }
`;
export const Lines = styled.div`
  position: absolute;
  top: -5%;
  left: -5%;
  width: 110%;
  height: 110%;
  background: repeating-linear-gradient(
    rgba(0, 0, 0, 0.725),
    rgba(0, 0, 0, 0.725) 2.5%,
    transparent 2.5%,
    transparent 5.25%
  );
  animation: ${lines} linear infinite 5s;
`;
export const Container = styled.div`
  width: 50vmin;
  height: 50vmin;
  min-height: 200px;
  min-width: 200px;
  position: relative;
  overflow: hidden;
  border-radius: 50%;
`;
export const Gradient = styled.div`
  height: 100%;
  background: linear-gradient(#a12bfb 25%, #fc2dfc, #eb209b 75%);
`;

export const Tree = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background: url(${palm}) 55% 5px no-repeat;
  background-size: 70%;
  filter: brightness(0%) blur(0.5px);
`;
