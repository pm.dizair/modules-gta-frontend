import React from "react";
import { Head, LoadItem, Table, Wrapper } from "./style";

export const TableLoading = ({ col = 10, length = 10, children }) => {
  return (
    <Wrapper>
      {children}
      <Table>
        <Head>
          <LoadItem className="skeleton-box-neon" style={{ height: "100%" }}>
            <td colSpan={col} />
          </LoadItem>
        </Head>
        <tbody>
          {[...Array(length)].map((x, i) => (
            <LoadItem
              className="skeleton-box-neon"
              key={i}
              style={{ height: 64, borderRadius: 8 }}
            >
              <td colSpan={col} />
            </LoadItem>
          ))}
        </tbody>
      </Table>
    </Wrapper>
  );
};
