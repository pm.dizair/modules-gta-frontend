import styled from "styled-components";

export const LoadItem = styled.tr`
  display: table-row !important;
  border-radius: 6px;
  overflow: hidden;
`;
export const Table = styled.table`
  width: 100%;
  border-spacing: 0 4px;
  border-radius: 4px;
  overflow: hidden;
`;
export const Head = styled.thead`
  height: 64px;
`;
export const Wrapper = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  flex-direction: column;
  position: relative;
  width: 100%;
  max-height: 812px;
  overflow: auto;
`;
