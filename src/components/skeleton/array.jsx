import React from "react";
import styled from "styled-components";

const Wrapper = styled.div`
  width: 100%;
  height: 100%;
  border-radius: 8px;
`;
const Text = styled.div`
  height: 48px;
  width: 100%;
  border-radius: 8px;
  margin-top: 10px;
`;
const Flex = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
`;
export const SkeletonArray = (props) =>
  [...Array(props?.count || 10)].map((x, i) => (
    <Flex {...props} key={i}>
      <Wrapper className={props.neon ? "skeleton-box-neon" : "skeleton-box"}>
        {props?.children}
      </Wrapper>
      {props?.withText && <Text className="skeleton-box" />}
    </Flex>
  ));
