import React from "react";
import styled from "styled-components";

const Wrapper = styled.div`
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  border-radius: 8px;
`;
export const Skeleton = (props) => (
  <Wrapper
    {...props}
    className={
      props.className || props.neon ? "skeleton-box-neon" : "skeleton-box"
    }
  >
    {props?.children}
  </Wrapper>
);
