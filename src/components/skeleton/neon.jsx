import React from "react";
import Skeleton from "react-loading-skeleton";
import "react-loading-skeleton/dist/skeleton.css";

const NeonSkeleton = (props) => {
  return (
    <Skeleton
      {...props}
      baseColor={"var(--grey_dark)"}
      highlightColor={"var(--grey)"}
    />
  );
};

export default NeonSkeleton;
