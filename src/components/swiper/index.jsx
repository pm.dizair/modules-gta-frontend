import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import { useTranslations } from "@/helpers/useTranslations";
import { useMediaQuery } from "react-responsive";
import { useRef } from "react";
import { useNavigate } from "react-router";
import { IconArrowNext, IconArrowPrev } from "../icons";
import { OutlineViolent } from "../ui/buttons/violent-btn/outline";
import GameCardLoader from "./../cards/game/loading";

import {
  Button,
  ButtonLeft,
  Title,
  Wrapper,
  TopPanel,
  Navigation,
} from "./style.js";

import "swiper/css";

export const ArrowNext = ({ className, style, onClick }) => (
  <Button
    style={{ ...style }}
    onClick={onClick}
    className={className}
    title="next"
  >
    <IconArrowNext stroke="#fff" />
  </Button>
);

export const ArrowPrev = ({ className, style, onClick }) => (
  <ButtonLeft
    style={{ ...style }}
    onClick={onClick}
    className={className}
    title="prev"
  >
    <IconArrowPrev stroke="#fff" />
  </ButtonLeft>
);

const Slider = ({
  data,
  settings,
  children,
  loading,
  withoutBottomSpace,
  CardComponent,
  title,
  more,
  alwaysShowArrows,
  autoScroll = false,
  haveData,
  minLength,
  hideButton = false,
  pagination = false,
}) => {
  const isTablet = useMediaQuery({ maxWidth: 1024 });
  const t = useTranslations();
  const push = useNavigate();
  const ref = useRef();
  const sliderRef = useRef();

  if (!haveData) {
    return null;
  }
  // TODO
  return (
    <>
      {title && (
        <TopPanel withoutBottomSpace={withoutBottomSpace} className="top-panel">
          <Title>{t(title)}</Title>
          <Navigation>
            {(alwaysShowArrows || !isTablet) && minLength && (
              <>
                <ArrowPrev
                  className={"arrow_prev"}
                  onClick={() => sliderRef.current?.slidePrev()}
                />
                <ArrowNext
                  className={"arrow_next"}
                  onClick={() => sliderRef.current?.slideNext()}
                />
              </>
            )}
            {more && hideButton === false && (
              <OutlineViolent
                reference={ref}
                onClick={() => {
                  window.scrollTo(0, 0);
                  push(more);
                }}
              >
                {t("show_more_games")}
              </OutlineViolent>
            )}
          </Navigation>
        </TopPanel>
      )}
      <Wrapper className="swiper_wrap">
        <Swiper
          {...settings}
          onSwiper={(e) => (sliderRef.current = e)}
          className="slider_container"
        >
          {loading ? (
            [...Array(6)].map((_, i) => (
              <SwiperSlide key={i}>
                {typeof CardComponent === "function" ? (
                  <CardComponent />
                ) : (
                  <GameCardLoader />
                )}
              </SwiperSlide>
            ))
          ) : !autoScroll ? (
            <>{children}</>
          ) : (
            <>{children}</>
          )}
        </Swiper>
        {pagination && <div className="swiper-pagination" />}
      </Wrapper>
    </>
  );
};

export default Slider;
