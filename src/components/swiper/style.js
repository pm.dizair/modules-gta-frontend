import styled from "styled-components";
import { Link } from "react-router-dom";

export const Wrapper = styled.div`
  width: 100%;
  height: 100%;

  overflow: auto;
  overflow-y: hidden;

  .slider_container {
    overflow: hidden;
  }

  ${(props) => props.height && `height: ${props.height}`};
  .swiper-pagination {
    z-index: 2000;
    margin: 16px auto 0;
    width: fit-content;
  }

  .swiper-pagination-bullet {
    width: 10px;
    height: 10px;
    background-color: var(--grey);
    opacity: 0.5;
    border-radius: 50%;
    display: inline-block;
    margin: 0 5px;
    cursor: pointer;
  }

  .swiper-pagination-bullet-active {
    opacity: 1;
  }

  @media screen and (max-width: 1024px) {
    .slider_container {
      overflow: visible;
      margin-left: 16px;
    }
  }

  @media screen and (max-width: 768px) {
    .slider_container {
      margin-left: 0px;
    }
  }
`;
export const TopPanel = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 0 0 20px 0;
  gap: 16px;
  button {
    font-size: 14px;
    padding: 12px 32px;
  }
  @media screen and (max-width: 1024px) {
    padding: 0 32px 20px 32px;
    gap: 10px;
  }
  @media screen and (max-width: 767px) {
    padding: 0 16px 20px 16px;
    button {
      font-size: 14px;
    }
  }
  @media screen and (max-width: 540px) {
    button {
      padding: 6px 10px;
      font-size: 14px;
    }
  }
  ${(props) =>
    props.withoutBottomSpace &&
    `
    padding: 6px 0 !important;
  `}
`;
export const Title = styled.h2`
  font-size: 32px;
  @media screen and (max-width: 767px) {
    font-size: 24px;
    line-height: 26px;
  }
  @media screen and (max-width: 540px) {
    max-width: 250px;
  }
`;
export const Button = styled.button`
  width: 48px;
  max-height: 48px;
  height: 48px;
  display: flex !important;
  align-items: center;
  justify-content: center;
  backdrop-filter: blur(50px);
  border: 1px solid var(--gold_neon_border);
  border-radius: 50%;
  z-index: 1;
  transition: 0.3s;
  padding: 0 !important;
  position: relative;
  ::after {
    content: "";
    width: 48px;
    position: absolute;
    top: -2px;
    left: -1px;
    right: -1px;
    bottom: -2px;
    border-radius: 50%;
    border: 3px solid var(--gold);
    z-index: -1000;
    filter: blur(4px);
  }
  &:hover {
    ::after {
      background: var(--gold_neon_hover);
    }
    @media screen and (max-width: 1024px) {
      box-shadow: none;
      ::after {
        background: transparent;
      }
    }
  }
  @media screen and (max-width: 767px) {
    height: 38px;
    width: 38px;
    ::after {
      width: 38px;
    }
  }
  @media screen and (max-width: 540px) {
    height: 32px;
    width: 32px;
    ::after {
      width: 32px;
      border-width: 2px;
    }
  }
`;
export const ButtonLeft = styled(Button)``;
export const SliderWrap = styled.div`
  ${(props) =>
    !props.isProvider &&
    `
  width: calc(100% + 16px);
  transform: translate(-8px, 0);

`}
  height: 100%;
  @media screen and (max-width: 1024px) {
    width: 100%;
    transform: translate(0, 0);
  }
`;

export const ShowMore = styled(Link)`
  padding: 8px 16px;
  display: flex;
  align-items: center;
  justify-content: center;
  height: 42px;
  background: #ffffff;
  border: 1px solid #f1509a;
  border-radius: 8px;
  font-weight: 500;
  font-size: 14px;
  max-width: 150px;
  line-height: 18px;
  letter-spacing: 0.3px;
  color: #f1509a;
  white-space: nowrap;
  transition: all 0.3s linear;
  &:hover {
    box-shadow: 0px 2px 7px rgb(0 0 0 / 40%);
    @media screen and (max-width: 1024px) {
      box-shadow: none;
    }
  }
  @media screen and (max-width: 767px) {
    font-size: 12px;
    line-height: 14px;
    border-radius: 6px;
  }
`;
export const LoadWrap = styled.div`
  width: 100%;
  overflow-x: scroll;
  display: flex;
  justify-content: flex-start;
  gap: 16px;
  ::-webkit-scrollbar {
    height: 0;
  }
  div {
    border-radius: 8px;
  }
  @media screen and (max-width: 1024px) {
    padding: 0 32px;
  }
  @media screen and (max-width: 767px) {
    padding: 0 16px;
  }
`;
export const List = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: flex-start;
  gap: 16px;
  overflow-y: auto;
  ::-webkit-scrollbar {
    width: 0px;
    background: #deeffa;
    height: 0;
  }
  ::-webkit-scrollbar-thumb {
    background: #a2c6dd;
    border-radius: 5px;
    cursor: pointer;
  }
  @media screen and (max-width: 1024px) {
    padding: 0 32px;
  }
  @media screen and (max-width: 767px) {
    padding: 0 16px;
  }
`;

export const Navigation = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  gap: 16px;
  align-items: center;

  padding: 8px 0;
`;
