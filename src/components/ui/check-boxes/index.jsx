import React from "react";
import styled from "styled-components";

const Input = styled.button`
  border: 1px solid #dedfe2;
  border-radius: 2px;
  width: 16px;
  position: relative;
  height: 16px;
  transition: all 0.3s linear;
  ${(props) =>
    props.checked &&
    `background: var(--gold);
  &::before {
    content: "";
    position: absolute;
    top: 1px;
    left: 1px;
    transform: rotate(-45deg);

    height: 8px;
    width: 12px;
    border-bottom: 2px solid #fff;
    border-left: 2px solid #fff;
  }`}
`;
export const CheckBox = ({ check, setCheck }) => {
  return (
    <Input
      type="button"
      title="button"
      checked={check}
      onClick={() => {
        setCheck(!check);
      }}
    />
  );
};
