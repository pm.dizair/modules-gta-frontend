import React from "react";
import { Loader } from "./style";

const CircleLoader = () => {
  return (
    <Loader className="spin-container">
      <div className="loading"></div>
    </Loader>
  );
};

export default CircleLoader;
