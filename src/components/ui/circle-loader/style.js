import styled, { keyframes } from "styled-components";

export const Spin = keyframes`

  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
`;

export const Loader = styled.div`
  display: flex;
  justify-content: space-around;
  align-items: center;
  margin-bottom: 20px;
  width: 100%;
  min-height: 50px;
  background-color: transparent;
  border-width: 1px;
  border-style: solid;
  color: var(--gold);
  border-image-source: linear-gradient(95.37deg, #f5763f 0%, #ff9200 111.75%) 1;
  backdrop-filter: blur(5px);
  border-radius: 8px;

  .loading {
    border-radius: 50%;
    width: 24px;
    height: 24px;
    border: 2px solid var(--gold);
    border-top-color: rgb(242, 139, 0);
    animation: ${Spin} 1s infinite linear;
  }
`;
