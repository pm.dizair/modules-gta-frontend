const removeNullableFields = (data) => {
  let result = {};

  if (typeof data === "object") {
    for (const field in data) {
      if (data[field]) {
        result = {
          ...result,
          [field]: data[field],
        };
      }
    }
    return result;
  }

  return null;
};

export default removeNullableFields;
