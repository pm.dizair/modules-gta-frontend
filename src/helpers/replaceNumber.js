const getReplaceNumber = (value, step = 3) => {
  return (
    Number(value)
      .toFixed(step)
      .replace(/\.?0+$/, '') || 0
  );
};
export default getReplaceNumber;
