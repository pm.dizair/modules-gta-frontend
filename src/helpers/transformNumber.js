const transformNumber = (number) => {
  const parts = number?.toString()?.split(".");
  if (parts?.length > 1) {
    if (parts[1] === "0") return parts[0];
    return `${parts[0]}.${parts[1][0]}`;
  }
  return number;
};

const getReducedNumber = (value) => {
  try {
    const number = Number(value);
    switch (true) {
      case number >= 1000000000:
        return `${transformNumber(number / 1000000000)}B`;
      case number >= 1000000:
        return `${transformNumber(number / 1000000)}M`;
      case number >= 10000:
        return `${transformNumber(number / 1000)}k`;
      default:
        return number || 0;
    }
  } catch (e) {}
};

export default getReducedNumber;
