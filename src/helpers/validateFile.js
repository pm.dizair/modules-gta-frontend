export const checkFile = (file) => {
  const validFormat = ["jpg", "jpeg", "png", "jfif"];
  const size = 10485760; // 10mb
  if (!validFormat.includes(file.name.split(".").pop())) {
    throw new Error("Invalid format");
  }
  if (file.size > size) {
    throw new Error("The size must be less than or equal to 10Mb");
  }
};
