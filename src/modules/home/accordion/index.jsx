import React, { useEffect, useRef, useState } from "react";
import { Item } from "./item";

import { useMediaQuery } from "react-responsive";
import { getImage } from "@/helpers/getImage";
import parse from "html-react-parser";
import { useGetQuestionQuery } from "@/api/content";
import { useCommon } from "@/store/selectors";
import { AccordionLoader } from "./loading";
// import car from "@/assets/images/car.webp";
import bg_desktop from "@/assets/new/images/backgrounds/main/faq_bg_desk.webp";
import bg_mobile from "@/assets/new/images/backgrounds/main/faq_bg_mob.webp";
import {
  Column,
  Text,
  Title,
  Img,
  AccordionWrap,
  Background,
  // Car,
  Wrap,
} from "./style";
import { TabletOff, TabletOn } from "@/helpers/responsive";
export const Accordion = () => {
  const point = useMediaQuery({ maxWidth: 1280 });
  const [calcHeight, setCalcHeight] = useState(600);

  const { language } = useCommon();
  const ref = useRef();

  const {
    data: question,
    isFetching,
    isLoading,
  } = useGetQuestionQuery({
    language,
  });

  const { title, description, image } = question?.leftQuestion?.[0] || {};

  useEffect(() => {
    if (!isLoading && ref?.current) {
      setCalcHeight(ref?.current?.offsetHeight);
    }
  }, [isLoading]);

  return (
    <Wrap calcHeight={calcHeight}>
      <TabletOff>
        <Background src={bg_desktop} alt="" loading="lazy" />
      </TabletOff>
      <TabletOn>
        <Background src={bg_mobile} alt="" loading="lazy" />
      </TabletOn>
      <AccordionWrap ref={ref}>
        {!isFetching && question?.rightQuestion?.length ? (
          <>
            <Column>
              <div className="left-text">
                {!point && <Title>{title}</Title>}
                <Text>{parse(description)}</Text>
              </div>
              <Img src={getImage(image)} loading="lazy" alt="" />
            </Column>
            <Column>
              {question?.rightQuestion?.map((cur) => (
                <Item key={cur.id} information={cur} id={cur.id} />
              ))}
            </Column>
            {point && <Title>{title}</Title>}
          </>
        ) : (
          <AccordionLoader />
        )}
        {/* TODO design */}
        {/* <Car src={car} alt="" />  TODO*/}
      </AccordionWrap>
    </Wrap>
  );
};
