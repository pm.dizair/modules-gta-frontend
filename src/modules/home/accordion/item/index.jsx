import React from "react";
import { Details, Summary, Text } from "./style";
import parse from "html-react-parser";

export const Item = ({ information }) => {
  const smoothMove = (e) => {
    e.stopPropagation();
    const target = e.currentTarget;
    const children = e.currentTarget.parentElement.children;
    if (children) {
      for (let child of children) {
        const desc = child.getElementsByClassName("desc")[0];
        const title = child.getElementsByClassName("title")[0];
        // max height is 150 minus 32 (it's padding)
        if (desc.scrollHeight >= 118) {
          desc.style.overflowY = "auto";
        } else {
          desc.style.overflowY = "hidden";
        }
        if (desc && child.classList.contains("active") && title) {
          desc.style.height = 0;
          title.style.borderBottomLeftRadius = 8 + "px";
          title.style.borderBottomRightRadius = 8 + "px";
        }
        if (child !== target) child.classList.remove("active");
      }
    }
    // when we have active element we add this styles
    target.classList.toggle("active");
    const desc = target.getElementsByClassName("desc")[0];
    const title = target.getElementsByClassName("title")[0];
    if (desc && target.classList.contains("active") && title) {
      desc.style.height = desc.scrollHeight + 32 + "px";
      title.style.borderBottomLeftRadius = 0;
      title.style.borderBottomRightRadius = 0;
    } else {
      desc.style.height = 0;
      title.style.borderBottomLeftRadius = 8 + "px";
      title.style.borderBottomRightRadius = 8 + "px";
    }
  };

  return (
    <Details onClick={(e) => smoothMove(e)}>
      <Summary className="title">
        <p>{information?.title}</p>
      </Summary>
      <Text onClick={(e) => e.stopPropagation()} className="desc">
        {parse(information?.description)}
      </Text>
    </Details>
  );
};
