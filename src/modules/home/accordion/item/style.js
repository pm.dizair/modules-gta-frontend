import styled from "styled-components";

export const Details = styled.div`
  padding: 4px 0;
  transition: all 0.5s;
  &.active {
    .desc {
      opacity: 1;
      padding: 16px;
    }
    .title {
      color: #025a3b;
      ::before {
        transform: rotateZ(-45deg);
        background: #ffffff;
      }
      ::after {
        transform: rotateZ(45deg);
        background: #ffffff;
      }
    }
  }
`;
export const Summary = styled.div`
  list-style: none;
  color: #fff;
  cursor: pointer;
  padding: 32px;
  width: 100%;
  border: none;
  text-align: left;
  outline: none;
  transition: 0.4s;
  position: relative;
  background: rgb(74 67 78 / 90%);
  border-radius: 8px;
  ${(props) =>
    props.open &&
    `border-bottom-left-radius: 0; border-bottom-right-radius: 0;`};
  &::after,
  ::before {
    content: "";
    width: 20px;
    height: 1.5px;
    position: absolute;
    transform: translateY(-50%);
    top: 50%;
    right: 34px;
    background-color: #fff;
    transition: all 0.5s linear;
    ${(props) => props.open && `transform: rotate(-180deg);`}
  }
  &::after {
    transform: rotate(${(props) => (props.open ? "-180deg" : "-90deg")});
  }
  p {
    margin: 0;
    padding: 0;
    font-weight: 400;
    word-break: break-word;
    width: calc(100% - 50px);
    color: #fff;
    font-size: 18px;
    line-height: 24px;
  }
  @media screen and (max-width: 1600px) {
    padding: 24px;
  }
  @media screen and (max-width: 540px) {
    padding: 20px;
    p {
      font-size: 14px;
      font-weight: 500;
      line-height: 20px;
    }
  }
`;
export const Text = styled.div`
  margin: 0;
  background: rgba(255, 255, 255, 0.1);
  backdrop-filter: blur(5px);
  padding: 0 16px;
  height: 0;
  overflow: hidden;
  max-height: 150px;
  transition: all 0.5s ease-out;
  border-bottom-left-radius: 8px;
  border-bottom-right-radius: 8px;
  position: relative;
  p {
    color: #fff;
    font-size: 14px;
    line-height: 20px;
    font-weight: 400;
  }
  &::-webkit-scrollbar {
    width: 2px;
    background-color: #ffffff00;
    height: 0;
  }
  &::-webkit-scrollbar-thumb {
    background-color: var(--gold);
    border-radius: 2px;
  }
`;
