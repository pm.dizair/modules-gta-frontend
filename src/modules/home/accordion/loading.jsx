import React from "react";
import { Column, Text, Title } from "./style";
import { useMediaQuery } from "react-responsive";
import NeonSkeleton from "@/components/skeleton/neon";

export const AccordionLoader = () => {
  const point = useMediaQuery({ maxWidth: 1280 });

  return (
    <>
      <Column>
        <div>
          {!point && (
            <Title>
              <NeonSkeleton />
            </Title>
          )}
          <Text>
            <NeonSkeleton count={2} />
          </Text>
        </div>
        <NeonSkeleton width={"100%"} height="70%" />
      </Column>
      <Column>
        {[...Array(5)].map((x, i) => (
          <NeonSkeleton height={72} key={i} />
        ))}
      </Column>
      {point && (
        <Title>
          <NeonSkeleton />
        </Title>
      )}
    </>
  );
};
