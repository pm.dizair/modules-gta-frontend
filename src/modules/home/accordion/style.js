import styled from "styled-components";
import { Wrapper } from "@/components/containers/container";

export const AccordionWrap = styled(Wrapper)`
  display: flex;
  width: 100%;
  overflow: hidden;
  min-height: 750px;
  padding-top: 35px;
  padding-bottom: 35px;
  gap: 40px;
  background-color: transparent;
  margin: 0 auto;
  @media screen and (max-width: 1440px) {
    min-height: 700px;
  }
  @media screen and (max-width: 1280px) {
    flex-direction: column-reverse;
    justify-content: flex-end;
    padding-top: 32px;
    padding-bottom: 15px;
    gap: 20px;
  }
`;
export const Column = styled.div`
  &:nth-child(1) {
    width: 40%;
  }
  &:nth-child(2) {
    width: 60%;
    padding-bottom: 30px;
  }
  .left-text {
    display: flex;
    flex-direction: column;
    gap: 16px;
  }

  div {
    a {
      color: #ce86ff;
      font-size: 14px;
    }
    p {
      margin: 0;
    }
  }
  @media screen and (max-width: 1440px) {
    &:nth-child(1) {
      width: 35%;
    }
    &:nth-child(2) {
      width: 65%;
    }
  }
  @media screen and (max-width: 1280px) {
    &:nth-child(1),
    &:nth-child(2) {
      width: 100%;
    }
    &:first-child {
      display: flex;
      align-items: center;
      justify-content: space-between;
    }
  }
  @media screen and (max-width: 600px) {
    &:first-child {
      display: flex;
      flex-direction: column;
      justify-content: flex-end;
      margin-bottom: 16px;
    }
  }
`;

export const Title = styled.h1`
  font-size: 36px;
  font-weight: 700;
  color: #fff;
  @media screen and (max-width: 767px) {
    font-size: 32px;
  }
  @media screen and (max-width: 540px) {
    font-size: 24px;
    line-height: 32px;
  }
`;

export const Text = styled.div`
  p {
    display: inline;
    font-size: 16px;
    margin-top: 20px;
    color: #fff;
    font-weight: 400;
    span {
      color: var(--gold);
      cursor: pointer;
    }
    a {
      font-size: 16px;
    }
  }
`;

export const Img = styled.img`
  width: 90%;
  max-height: 100%;
  z-index: 100;
  @media screen and (max-width: 1280px) {
    max-width: 400px;
  }
  @media screen and (max-width: 767px) {
    width: 100%;
    margin-top: 20px;
  }
`;

export const Wrap = styled.div`
  width: 100%;
  overflow: hidden;
  position: relative;
  padding-top: 80px;
  transform: translateY(20px);
  z-index: 1;
`;
export const Background = styled.img`
  position: absolute;
  top: -30px;
  left: 0;
  width: 100%;
  height: 100%;
  height: ${(props) => `calc(${props.calcHeight}px + 400px)`};
  object-fit: cover;
  object-position: top;
  z-index: -10000;
  @media screen and (max-width: 1280px) {
    height: ${(props) => `calc(${props.calcHeight}px + 600px)`};
  }
`;
export const Car = styled.img`
  position: absolute;
  width: 570px;
  height: auto;
  bottom: -19px;
  right: 0;
  z-index: -10;
  filter: drop-shadow(0px 0px 20px rgba(11, 206, 250, 0.5));
  transform: translate3d(0, 0, 0);
  @media screen and (max-width: 1280px) {
    display: none;
  }
`;
