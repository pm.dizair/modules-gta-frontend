import React, { useEffect, useRef } from "react";
import { Container, Content, Flex, Band, Wrap } from "./style";
import { SubTitle, Text, Title } from "../style";
import { ColorButton } from "@/components/ui";
import { useNavigate } from "react-router";
import ImgBackground from "@/assets/new/images/backgrounds/main/bg.webp";
import casino from "@/assets/new/images/persona/main777.webp";
import { useTriggerData } from "@/helpers/useTriggerData";
import { useCommon, useUser } from "@/store/selectors";
import { useModal } from "@ebay/nice-modal-react";
import { MODALS } from "@/components/modals/const";
import { LINK_TEMPLATES } from "@/const/link-templates";
import { useGetPromotionRandomMutation } from "@/api.v2/promotion";
import { useTranslations } from "@/helpers/useTranslations";
import parse from "html-react-parser";
import { useClaimWeekendBonusMutation } from "@/api.v2/user";
import useAnalyticsEventTracker from "@/helpers/useAnalyticsEventTracker";
import Load from "./load";

export const Banner = () => {
  const [getPromotionRandom, { data: promotion, isLoading, isUninitialized }] =
    useGetPromotionRandomMutation();
  const push = useNavigate();
  const { isAuth } = useUser();
  const t = useTranslations();
  const { language } = useCommon();

  const img = useRef();

  const { show: showRegister } = useModal(MODALS.REGISTER);
  const [claimWeekendBonus] = useClaimWeekendBonusMutation();
  const gaEventTracker = useAnalyticsEventTracker();

  const triggerData = useTriggerData(promotion?.bonus_trigger, {
    handler: () => {
      claimWeekendBonus();
      push(LINK_TEMPLATES.GAME_LISTING({ language }));
    },

    text: promotion?.button_text
      ? promotion?.button_text
      : t("home_page_no_promo_button"),
  });

  const getTriggerData = () => {
    if (isAuth) {
      return triggerData;
    } else {
      return {
        text: t("promotion_button_create_account"),
        handler: showRegister,
      };
    }
  };

  const { text, handler } = getTriggerData();

  useEffect(() => {
    getPromotionRandom({
      language,
      position: "main_page",
    });
  }, [language, isAuth]);
  return (
    <Container>
      <img src={ImgBackground} alt="" className="background-image" />
      <Content>
        <Flex>
          {isLoading || isUninitialized ? (
            <Load />
          ) : promotion?.id ? (
            <>
              {promotion?.title && (
                <SubTitle>{parse(promotion?.title)}</SubTitle>
              )}
              {promotion?.sub_title && (
                <Title>{parse(promotion?.sub_title)}</Title>
              )}
              {promotion?.description && (
                <Text>{parse(promotion?.description)}</Text>
              )}
            </>
          ) : (
            <>
              <SubTitle>
                {t("home_default_promotion_banner_sub_title")}
              </SubTitle>
              <Title>{t("home_default_promotion_banner_title")}</Title>
              <Text>{t("home_default_promotion_banner_description")}</Text>
            </>
          )}
          {isLoading || isUninitialized ? (
            <ColorButton />
          ) : text || handler ? (
            <ColorButton
              title={text}
              onClick={() => {
                if (promotion) {
                  gaEventTracker(
                    "Main page " + promotion?.title,
                    "Baners",
                    "click"
                  );
                  if (promotion.deposit_type === "weekend") {
                    claimWeekendBonus();
                  }
                }
                handler();
              }}
            />
          ) : (
            (text || handler) && <ColorButton title={text} onClick={handler} />
          )}
        </Flex>
        <Wrap>
          <Band ref={img} src={casino} alt="" />
        </Wrap>
      </Content>
    </Container>
  );
};
