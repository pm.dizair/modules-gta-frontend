import React from "react";
import { SubTitle, Text, Title } from "../style";
import NeonSkeleton from "@/components/skeleton/neon";

const Load = () => {
  return (
    <>
      <SubTitle>
        <NeonSkeleton width={280} />
      </SubTitle>
      <Title>
        <NeonSkeleton width={280} count={2} />
      </Title>
      <Text>
        <NeonSkeleton width={280} />
      </Text>
    </>
  );
};

export default Load;
