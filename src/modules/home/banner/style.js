import { overflowText } from "@/assets/css/global";
import styled from "styled-components";

export const Container = styled.section`
  z-index: 1;
  overflow: hidden;
  position: relative;
  width: 100vw;
  height: calc(100vh - 150px);
  max-height: 800px;

  z-index: 2;
  .background-image {
    width: 100%;
    height: 110%;
    object-fit: cover;
    top: -5%;
    background-color: var(--black);
    position: absolute;
    z-index: -2;
  }
  @media screen and (min-width: 1920px) {
    width: 100%;
    .background-image {
      width: 100%;
    }
  }
  @media screen and (max-width: 1024px) {
    opacity: 1 !important;
    height: fit-content;
    max-height: fit-content;
  }
`;

export const Content = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  align-items: flex-end;
  justify-content: space-between;
  padding: 0 130px 0 220px;
  @media screen and (min-width: 1920px) {
    max-width: 1920px;
    margin: auto;
  }
  @media screen and (max-width: 1600px) {
    padding: 0 90px 0 120px;
  }
  @media screen and (max-width: 1440px) {
    padding: 0 64px;
  }
  @media screen and (max-width: 1024px) {
    padding: 80px 32px 0 32px;
    flex-direction: column;
    align-items: center;
  }
  @media screen and (max-width: 767px) {
    padding: 80px 16px 0 16px;
  }
`;

export const Flex = styled.div`
  height: 100%;
  display: flex;
  width: 100%;
  justify-content: center;
  align-items: flex-start;
  flex-direction: column;
  max-width: 500px;
  .color-btn {
    height: 48px;
    display: block;
    min-width: 120px;
    font-size: 16px;
    line-height: 18px;
    ${overflowText}
    width: auto;
    max-width: 300px;
    z-index: 1;
  }
  @media screen and (max-width: 1024px) {
    display: flex;
    flex-direction: column;
    width: 100%;
    align-items: center;
    justify-content: center;
    .color-btn {
      z-index: 10;
    }
  }
  @media screen and (max-width: 767px) {
    .color-btn {
      font-size: 14px;
      height: 48px;
    }
  }
  @media screen and (max-width: 540px) {
    .color-btn {
      font-size: 14px;
      height: 42px;
    }
  }
`;

export const Band = styled.img`
  object-fit: contain;
  position: absolute;
  bottom: 0;
  object-position: bottom;
  height: 710px;
  right: -45px;
  // minus size-header
  max-height: calc(100% - 140px);

  @media screen and (max-width: 1280px) {
    height: 600px;
  }
  @media screen and (max-width: 1024px) {
    position: static;
    transform: none;
    width: 100%;
    max-width: 500px;
    height: auto;
    min-height: 300px;
  }
  @media screen and (max-width: 540px) {
    width: calc(100% + 100px);
  }
`;

export const Wrap = styled.div`
  position: relative;
  height: 100%;
  width: 100%;
  @media screen and (max-width: 1024px) {
    display: flex;
    justify-content: center;
  }
`;
