import React from "react";
import parse from "html-react-parser";

import { useNavigate } from "react-router";
import {
  Title,
  Wrapper,
  Lines,
  Description,
  Flex,
  Person,
  Gradient,
} from "./style";
import lines from "@/assets/new/images/backgrounds/line.webp";
import { ColorButton } from "@/components/ui";
import { getImage } from "@/helpers/getImage";
import { useCommon } from "@/store/selectors";
import promo_bg from "@/assets/new/images/backgrounds/promotions/banners/bg_2.webp";
import promo_person from "@/assets/new/images/promotions/pers16.webp";

const PromotionCard = ({ item }) => {
  const push = useNavigate();
  const { language } = useCommon();

  return (
    <Wrapper
      img={
        !item?.background_promotion_image
          ? promo_bg
          : getImage(item?.background_promotion_image)
      }
    >
      <Flex>
        <Title>{item?.title}</Title>
        <Description> {parse(item?.sub_title)}</Description>
        <ColorButton
          title={item?.button_text}
          onClick={() => {
            push(`/${language}/promotions/detail/${item?.id}`);
            window.scrollTo(0, 0);
          }}
        />
      </Flex>
      <Person
        src={!item?.person ? promo_person : getImage(item?.person)}
        loading="lazy"
        alt=""
      />
      <Lines src={lines} alt="" />
      <Gradient />
    </Wrapper>
  );
};
export default PromotionCard;
