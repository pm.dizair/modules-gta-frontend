import { Title, Description, Flex, Wrapper, Lines } from "./style";
import lines from "@/assets/new/images/backgrounds/line.webp";
import NeonSkeleton from "@/components/skeleton/neon";

const PromotionCardLoader = () => {
  return (
    <Wrapper style={{ backgroundColor: "var(--black)" }}>
      <Flex>
        <Title>
          <NeonSkeleton />
        </Title>
        <Description>
          <NeonSkeleton />
        </Description>
        <NeonSkeleton width={120} height={42} />
      </Flex>
      <Lines src={lines} alt="" />
    </Wrapper>
  );
};
export default PromotionCardLoader;
