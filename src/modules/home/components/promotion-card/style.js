import styled from "styled-components";
import { GTA_TEXT } from "@/assets/css/global";

export const Wrapper = styled.div`
  width: 100%;
  margin-top: 20px;
  height: 100%;
  overflow: visible;
  position: relative;
  padding: 0 48px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  z-index: 3;
  border-radius: 6px;
  background: ${(props) => `url(${props.img})`};
  background-size: cover;
  background-position: center;
  ::after {
    content: "";
    inset: 0;
    width: 100%;
    height: 100%;
    z-index: -1;
    position: absolute;
    background: rgba(20 9 26 0.4);
  }
  @media screen and (max-width: 1600px) {
    padding: 0px 32px;
  }
  @media screen and (max-width: 1280px) {
    padding: 0px 16px;
  }
  @media screen and (max-width: 540px) {
    width: calc(100vw - 32px);
    margin-top: 16px;
    justify-content: flex-start;
  }
`;
export const Flex = styled.div`
  width: 100%;
  max-width: 420px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  gap: 20px;
  position: relative;
  z-index: 4;
  .color-btn {
    height: 42px;
    font-size: 14px;
    padding: 12px 32px;
  }
  @media screen and (max-width: 1920px) {
    max-width: 55%;
  }
  @media screen and (max-width: 1600px) {
    .color-btn {
      font-size: 14px;
      padding: 6px 18px;
    }
  }
  @media screen and (max-width: 1280px) {
    max-width: 260px;
  }
  @media screen and (max-width: 540px) {
    max-width: 100%;
    padding-top: 30px;
    gap: 10px;
    padding-bottom: 30px;
    text-align: center;
    justify-content: flex-start;
    .color-btn {
      align-self: center;
    }
  }
`;
export const Lines = styled.img`
  position: absolute;
  bottom: 0;
  right: 0px;
  height: 100%;
  z-index: 2;
  @media screen and (max-width: 540px) {
    height: 80%;
  }
`;
export const Person = styled.img`
  position: absolute;
  bottom: 0;
  right: 48px;
  z-index: 3;
  height: calc(100% + 20px);
  @media screen and (max-width: 1600px) {
    right: 32px;
  }
  @media screen and (max-width: 1280px) {
    right: 16px;
  }
  @media screen and (max-width: 540px) {
    height: calc(100% - 160px);
    right: 60%;
    max-width: 80%;
    transform: translateX(75%);
    object-fit: contain;
  }
`;

export const Title = styled.h3`
  font-family: "Kaushan Script";
  font-weight: 400;
  font-size: 20px;
  line-height: 24px;
  color: #ffffff;
  @media screen and (max-width: 1440px) {
    font-size: 14px;
    line-height: 18px;
  }
`;
export const Description = styled.div`
  margin: 0;
  height: auto !important;
  padding: 0 !important;
  font-weight: 400;
  font-size: 42px;
  line-height: 49px;
  color: #ffffff;
  ${GTA_TEXT}
  div {
    height: auto !important;
    padding: 0 !important;
  }
  @media screen and (max-width: 1920px) {
    font-size: 36px;
    line-height: 48px;
  }
  @media screen and (max-width: 1600px) {
    font-size: 36px;
    line-height: 36px;
  }
  @media screen and (max-width: 1280px) {
    font-size: 24px;
    line-height: 28px;
  }
`;
export const Img = styled.img`
  position: absolute;
  min-width: 100%;
  top: 0;
  left: 0;
  z-index: -21;
`;
export const Gradient = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  left: 0;
  top: 0;
  background-color: rgb(20 9 26 / 50%);
  z-index: 1;
`;
