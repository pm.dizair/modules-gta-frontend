export { default as Recommended } from './recommended';
export { default as NewGames } from './new-game';
export { default as Providers } from './provider';
export { default as LiveGames } from './live';
export { default as Promotions } from './promotion';

export default {};
