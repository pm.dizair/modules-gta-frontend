import React from "react";
import { GameCard } from "@/components/cards";
import { games_settings } from "../settings";
import { LiveGamesWrap } from "../style";
import { useCommon } from "@/store/selectors";
import { useGetGamesMutation } from "@/api.v2/games";
import { LINK_TEMPLATES } from "@/const/link-templates";
import Slider from "@/components/swiper";
import { SwiperSlide } from "swiper/react";
import { useFetchGames } from "@/modules/home/helpers";
import {
  HOME_PAGE_GAMES_CATEGORIES,
  HOME_PAGE_GAMES_LIMITS,
} from "@/modules/home/const";

const LiveGames = () => {
  const { language } = useCommon();

  const [getGames] = useGetGamesMutation();

  const { renderedItems, isLoading, isError } = useFetchGames(
    `live-casino-games`,
    getGames,
    {
      page: 1,
      limit: HOME_PAGE_GAMES_LIMITS.LIVE_CASINO,
      category: HOME_PAGE_GAMES_CATEGORIES.LIVE_CASINO,
    }
  );

  if (isError) {
    return null;
  }

  return (
    <LiveGamesWrap>
      <Slider
        settings={games_settings}
        haveData={isLoading || renderedItems.length}
        more={LINK_TEMPLATES.LIVE_CASINO_LISTING({
          language,
        })}
        title={"main_page_live_casino_games"}
        withTextLoading
        loading={isLoading}
        minLength={renderedItems.length > 5}
      >
        {renderedItems.map((item, index) => (
          <SwiperSlide key={index}>
            <GameCard favorite item={item} big />
          </SwiperSlide>
        ))}
      </Slider>
    </LiveGamesWrap>
  );
};

export default LiveGames;
