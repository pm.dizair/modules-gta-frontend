import React from "react";
import { useCommon } from "@/store/selectors";
import { LINK_TEMPLATES } from "@/const/link-templates";
import { useGetGamesMutation } from "@/api.v2/games";
import { GameCard } from "@/components/cards";
import { games_settings } from "../settings";
import { Wrapper } from "../style";
import Slider from "@/components/swiper";
import { SwiperSlide } from "swiper/react";
import { useFetchGames } from "@/modules/home/helpers";
import {
  HOME_PAGE_GAMES_CATEGORIES,
  HOME_PAGE_GAMES_LIMITS,
} from "@/modules/home/const";

const NewGames = () => {
  const { language } = useCommon();

  const [getGames] = useGetGamesMutation();

  const { renderedItems, isLoading, isError } = useFetchGames(
    `new-games`,
    getGames,
    {
      page: 1,
      limit: HOME_PAGE_GAMES_LIMITS.NEW_GAMES,
      category: HOME_PAGE_GAMES_CATEGORIES.NEW_GAMES,
    }
  );

  if (isError) {
    return null;
  }

  return (
    <Wrapper>
      <Slider
        settings={games_settings}
        more={LINK_TEMPLATES.GAME_LISTING({
          language,
          category: HOME_PAGE_GAMES_CATEGORIES.NEW_GAMES,
        })}
        withTextLoading
        loading={isLoading}
        title={"main_page_new_games"}
        minLength={renderedItems.length > 5}
        haveData={isLoading || renderedItems.length}
      >
        {renderedItems.map((item, index) => (
          <SwiperSlide key={index}>
            <GameCard favorite item={item} new_game />
          </SwiperSlide>
        ))}
      </Slider>
    </Wrapper>
  );
};
export default NewGames;
