import React, { useEffect } from "react";
import PromotionCard from "@/modules/home/components/promotion-card";
import { promotions_settings } from "../settings";
import { PromotionWrap } from "../style";
import { useCommon, useUser } from "@/store/selectors";
import { useGetPromotionListMutation } from "@/api.v2/promotion";
import Slider from "@/components/swiper";
import { SwiperSlide } from "swiper/react";
import PromotionCardLoader from "../../promotion-card/loading";

export const Promotions = () => {
  const { user } = useUser();

  const [getPromotionList, { data: promotions, isLoading, isError }] =
    useGetPromotionListMutation();

  const { language } = useCommon();

  useEffect(() => {
    getPromotionList({ language, page: 1, limit: 10, userId: user?.id });
  }, [language, user?.id]);

  if (isError) {
    return null;
  }
  return (
    <PromotionWrap>
      <Slider
        settings={promotions_settings}
        haveData={isLoading || !!promotions?.items?.length}
        more={`/${language}/promotions/1`}
        title={"main_page_promotions"}
        withTextLoading
        loading={isLoading}
        minLength={promotions?.items?.length > 2}
        hideButton={!(promotions?.items?.length > 2)}
        pagination
        CardComponent={PromotionCardLoader}
      >
        {promotions?.items?.map((cur) => (
          <SwiperSlide key={cur.id}>
            <PromotionCard item={cur} />
            {promotions?.items?.length === 1 && <div />}
          </SwiperSlide>
        ))}
      </Slider>
    </PromotionWrap>
  );
};
export default Promotions;
