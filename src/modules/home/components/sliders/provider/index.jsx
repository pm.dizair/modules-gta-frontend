import React from "react";
import { ProviderCard } from "@/components/cards";
import { providers_settings } from "../settings";
import { ProviderWrap } from "../style";
import { LINK_TEMPLATES } from "@/const/link-templates";
import { useCommon } from "@/store/selectors";
import Slider from "@/components/swiper";
import { SwiperSlide } from "swiper/react";
const Provider = () => {
  const { language, brands, isLoading } = useCommon();

  return (
    <ProviderWrap>
      <Slider
        settings={providers_settings}
        haveData={brands?.length}
        title={"main_page_providers"}
        withTextLoading
        more={LINK_TEMPLATES.GAME_LISTING({ language, category: "popular" })}
        loading={isLoading}
        minLength={brands?.length > 6}
      >
        {brands?.map((item) => (
          <SwiperSlide key={item.id}>
            <ProviderCard item={item} />
          </SwiperSlide>
        ))}
      </Slider>
    </ProviderWrap>
  );
};
export default Provider;
