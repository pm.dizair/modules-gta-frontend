import React from "react";
import { useGetGamesMutation } from "@/api.v2/games";
import { GameCard } from "@/components/cards";
import { games_settings } from "../settings";
import { RecommendedWrap } from "../style";
import { LINK_TEMPLATES } from "@/const/link-templates";
import { useCommon } from "@/store/selectors";
import Slider from "@/components/swiper";
import { SwiperSlide } from "swiper/react";
import { useFetchGames } from "@/modules/home/helpers";
import {
  HOME_PAGE_GAMES_CATEGORIES,
  HOME_PAGE_GAMES_LIMITS,
} from "@/modules/home/const";

const Recommended = () => {
  const { language } = useCommon();
  const [getGames] = useGetGamesMutation();

  const { renderedItems, isError, isLoading } = useFetchGames(
    "recommended",
    getGames,
    {
      page: 1,
      limit: HOME_PAGE_GAMES_LIMITS.RECOMMENDED,
      category: HOME_PAGE_GAMES_CATEGORIES.RECOMMENDED,
    }
  );

  if (isError) {
    return null;
  }

  return (
    <RecommendedWrap>
      <Slider
        settings={games_settings}
        haveData={isLoading || renderedItems.length}
        more={LINK_TEMPLATES.GAME_LISTING({ language, category: "popular" })}
        title={"main_page_recommended_for_you"}
        withTextLoading
        loading={isLoading}
        minLength={renderedItems.length > 5}
      >
        {renderedItems.map((item, index) => (
          <SwiperSlide key={index}>
            <GameCard favorite item={item} big />
          </SwiperSlide>
        ))}
      </Slider>
    </RecommendedWrap>
  );
};

export default Recommended;
