import styled from "styled-components";

export const Wrapper = styled.section`
  max-width: 1920px;
  width: 100%;
  padding: 0px 80px;
  margin: 0 auto;
  position: relative;
  .choose {
    justify-content: space-between;
    .tab {
      @media screen and (max-width: 1024px) {
        min-width: auto;
      }
    }
    .react-loading-skeleton {
      margin: 13px 0;
    }
  }
  z-index: 2;
  @media screen and (max-width: 1440px) {
    padding: 0px 64px;
  }
  @media screen and (max-width: 1024px) {
    padding: 0;
  }
  @media screen and (max-width: 767px) {
    .choose {
      .react-loading-skeleton {
        margin: 1px 0;
      }
      margin-bottom: 8px;
      .tab {
        margin-bottom: 8px;
      }
    }
  }
`;
export const TournamentWrap = styled(Wrapper)`
  @media screen and (max-width: 1024px) {
    padding: 0 16px;
  }
`;

export const RecommendedWrap = styled(Wrapper)`
  padding-top: 40px;
`;

export const ProviderWrap = styled(Wrapper)`
  width: 100%;
  .swiper,
  swiper-container {
    max-height: 180px;
  }
`;

export const LiveGamesWrap = styled(Wrapper)``;

export const TabSliderWrap = styled(Wrapper)`
  padding: 0;
`;

export const GamesWrap = styled(Wrapper)`
  padding: 0;
`;

export const TabsWrap = styled.div`
  width: 100%;
  margin-top: 32px;
`;
export const PromotionWrap = styled(Wrapper)`
  .swiper,
  swiper-container {
    height: 440px;
  }

  @media screen and (max-width: 1600px) {
    .swiper,
    swiper-container {
      height: 320px;
    }
  }
  @media screen and (max-width: 1280px) {
    .swiper,
    swiper-container {
      height: 280px;
    }
  }

  @media screen and (max-width: 540px) {
    .swiper,
    swiper-container {
      height: 340px;
    }
    .top-panel {
      padding: 0 16px;
    }
  }
`;
