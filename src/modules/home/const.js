export const HOME_PAGE_GAMES_LIMITS = {
  RECOMMENDED: 15,
  GAMES: 12,
  LIVE_CASINO: 15,
  NEW_GAMES: 15,
  TOP_WINS_DAY: 5,
  TOP_WINS_MONTH: 5,
  TOP_MONTHLY_GAMES: 18,
};

export const HOME_PAGE_GAMES_CATEGORIES = {
  RECOMMENDED: "recommended",
  LIVE_CASINO: "live",
  NEW_GAMES: "new",
};
