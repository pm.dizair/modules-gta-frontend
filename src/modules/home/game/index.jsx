import React, { useEffect, useState } from "react";
import GameCard from "@/components/cards/game";
import { TabletOff, TabletOn } from "@/helpers/responsive";
import { GamesWrap, Wrapper } from "../components/sliders/style";
import { games_settings } from "../components/sliders/settings";
import { ChooseTabs } from "@/components/tabs/choose-tabs";
import Slider from "@/components/swiper";
import { SwiperSlide } from "swiper/react";
import { Title, List, ButtonContainer, Wrap } from "./style";
import { OutlineViolent } from "@/components/ui/buttons/violent-btn/outline";
import { useNavigate } from "react-router";
import { useTranslations } from "@/helpers/useTranslations";
import { useCommon } from "@/store/selectors";
import { LINK_TEMPLATES } from "@/const/link-templates";
import { useGetGamesMutation } from "@/api.v2/games";
import GameCardLoader from "@/components/cards/game/loading";
import { useFetchGames } from "../helpers";
import { HOME_PAGE_GAMES_LIMITS } from "../const";

const Games = () => {
  const [category, setCategory] = useState(null);

  const push = useNavigate();
  const t = useTranslations();
  const { language, categories, isLoading: isCommonLoading } = useCommon();

  const [getGames] = useGetGamesMutation();

  const { renderedItems, isLoading } = useFetchGames(
    `games/${category}`,
    getGames,
    {
      page: 1,
      category,
      limit: HOME_PAGE_GAMES_LIMITS.GAMES,
    }
  );

  useEffect(() => {
    setCategory(categories[0]?.slug);
  }, []);

  return (
    <Wrapper>
      <Wrap>
        <Title>{t("main_page_games_filter")}</Title>
        <TabletOn>
          <OutlineViolent
            onClick={() => {
              window.scrollTo(0, 0);
              push(LINK_TEMPLATES.GAME_LISTING({ language, category }));
            }}
          >
            {t("show_more_games")}
          </OutlineViolent>
        </TabletOn>
      </Wrap>
      <ChooseTabs
        gaEventName={"Main page game category"}
        data={categories}
        active={category}
        setActive={setCategory}
        loading={isCommonLoading}
      />
      <TabletOff>
        <List>
          {isLoading
            ? [...Array(HOME_PAGE_GAMES_LIMITS.GAMES)].map((_, i) => (
                <GameCardLoader key={i} />
              ))
            : renderedItems
                ?.slice(0, HOME_PAGE_GAMES_LIMITS.GAMES)
                .map((item) => <GameCard favorite item={item} key={item.id} />)}
        </List>
      </TabletOff>
      <TabletOn>
        <GamesWrap>
          <Slider
            settings={games_settings}
            haveData={isLoading || renderedItems.length}
            more={LINK_TEMPLATES.GAME_LISTING({
              language,
              category: "popular",
            })}
            withTextLoading
            loading={isLoading}
          >
            {renderedItems?.map((item) => (
              <SwiperSlide key={item.id}>
                <GameCard favorite item={item} big />
              </SwiperSlide>
            ))}
          </Slider>
        </GamesWrap>
      </TabletOn>
      <TabletOff>
        <ButtonContainer>
          <OutlineViolent
            onClick={() => {
              window.scrollTo(0, 0);
              push(LINK_TEMPLATES.GAME_LISTING({ language, category }));
            }}
          >
            {t("show_more_games")}
          </OutlineViolent>
        </ButtonContainer>
      </TabletOff>
    </Wrapper>
  );
};

export default Games;
