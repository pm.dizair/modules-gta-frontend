import styled from "styled-components";

export const Wrapper = styled.div`
  width: 100%;
  margin: 40px 0;
`;

export const Wrap = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 0 0 24px 0;
  button {
    font-size: 14px;
    padding: 12px 32px;
  }
  @media screen and (max-width: 1024px) {
    padding: 0 32px 24px 0;
  }
  @media screen and (max-width: 767px) {
    padding: 0 16px 24px 0;
    button {
      font-size: 14px;
    }
  }
  @media screen and (max-width: 540px) {
    padding: 0 16px 16px 0;
    button {
      padding: 6px 10px;
      font-size: 14px;
    }
  }
`;
export const ButtonContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  padding-top: 24px;
  & > button {
    height: 48px;
    padding: 16px 40px;
  }
  @media screen and (max-width: 1440px) {
    & > button {
      padding: 12px 40px;
    }
  }
  @media screen and (max-width: 1024px) {
    width: auto;
    justify-content: flex-end;
    padding: 0 0 24px 0;
  }
`;

export const Title = styled.h1`
  font-size: 32px;

  @media screen and (max-width: 1024px) {
    padding-left: 32px;
  }
  @media screen and (max-width: 767px) {
    font-size: 24px;
    line-height: 36px;
    padding: 0 0 0 16px;
  }
`;

export const List = styled.div`
  width: 100%;
  display: flex;
  align-items: flex-start;
  flex-wrap: wrap;
  justify-content: flex-start;
  gap: 16px 12px;
  & > div {
    width: calc(100% / 6 - 10px);

    @media screen and (max-width: 1024px) {
      width: 100%;
    }
  }
`;
