import { useHomePageActions } from "@/store/actions/home";
import { homePageSlice } from "@/store/reducers/home";
import { useEffect } from "react";
import { useSelector } from "react-redux";

export const useFetchGames = (key, getGames, body) => {
  const { addItemsAction, setParamsAction } = useHomePageActions();

  const { positions, params, games } = useSelector(
    (state) => state[homePageSlice.name]
  );

  const fetchHandler = async () => {
    if (key && !positions[key]) {
      setParamsAction({ key, isLoading: true });
      const res = await getGames(body);

      if (res.data) {
        addItemsAction({ key, items: res.data.items });
      } else {
        setParamsAction({ key, isLoading: false, isError: true });
      }
    }
  };

  const renderItems = () => {
    if (positions[key]?.length) {
      return positions[key].map((id) => games[id]);
    }
    return [];
  };

  useEffect(() => {
    fetchHandler();
  }, [key, Boolean(positions[key])]);

  return {
    renderedItems: renderItems(),
    isLoading: Boolean(params[key]?.isLoading),
    isError: Boolean(params[key]?.isError),
  };
};
