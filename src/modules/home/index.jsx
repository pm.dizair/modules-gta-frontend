import { Accordion } from "./accordion";
import { Banner } from "./banner";
import {
  LiveGames,
  NewGames,
  Promotions,
  Providers,
  Recommended,
} from "./components/sliders";
import GameList from "./game";
import { RatingList } from "./rating-list";
import { Content, Shadow, WrapCurve } from "./style";
import TournamentCurrent from "./tournament";

const Home = () => {
  return (
    <>
      <Banner />
      <Content id="content">
        <Shadow />
        <Recommended />
        <GameList />
        <Providers />
        <LiveGames />
        <Promotions />
        <NewGames />
        <WrapCurve>
          <TournamentCurrent />
          <RatingList />
          <Accordion />
        </WrapCurve>
      </Content>
    </>
  );
};

export default Home;
