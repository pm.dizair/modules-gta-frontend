import { ColorButton } from "@/components/ui";
import React from "react";
import { Empty, EmptyTitle } from "../../style";

import emptyGirl from "@/assets/new/images/persona/girl-empty.webp";
import { useNavigate } from "react-router";
import { useCommon } from "@/store/selectors";
import { LINK_TEMPLATES } from "@/const/link-templates";

export const EmptyBlock = () => {
  const { language } = useCommon();
  const push = useNavigate();

  return (
    <Empty>
      <EmptyTitle>Do you want to be on the top of players?</EmptyTitle>
      <ColorButton
        title={"Play games"}
        onClick={() => {
          window.scrollTo(0, 0);
          push(LINK_TEMPLATES.GAME_LISTING({ language }));
        }}
      />
      <img src={emptyGirl} loading="lazy" alt="" />
    </Empty>
  );
};
