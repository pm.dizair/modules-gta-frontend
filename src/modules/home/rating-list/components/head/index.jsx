import { useTranslations } from "@/helpers/useTranslations";
import React from "react";

import { Head, Period } from "../../style";

export const HeadTab = ({ activeTab, setActiveTab }) => {
  const t = useTranslations();
  const tabs = [
    { id: "day", text: t("main_page_top_wins_day") },
    { id: "month", text: t("main_page_top_wins_month") },
  ];

  return (
    <Head>
      {tabs.map((cur, id) => (
        <Period
          title="button"
          active={activeTab === cur.id}
          onClick={() => {
            setActiveTab(cur.id);
          }}
          key={id}
        >
          {cur.text}
        </Period>
      ))}
    </Head>
  );
};
