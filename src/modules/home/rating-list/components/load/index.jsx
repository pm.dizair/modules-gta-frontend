import NeonSkeleton from "@/components/skeleton/neon";
import { useTranslations } from "@/helpers/useTranslations";
import { ItemLoader } from "../../items/item-rang/loading";
import { List } from "../../items/item-rang/style";
import { Head, Period } from "../../style";

export const Load = () => {
  const t = useTranslations();
  const tabs = [t("main_page_top_wins_day"), t("main_page_top_wins_month")];
  return (
    <>
      <Head>
        {tabs?.map((cur, id) => (
          <Period active={"Day"} key={id}>
            <NeonSkeleton width={100} />
          </Period>
        ))}
      </Head>
      <List>
        {[...Array(5)].map((x, i) => (
          <ItemLoader key={i} />
        ))}
      </List>
    </>
  );
};
