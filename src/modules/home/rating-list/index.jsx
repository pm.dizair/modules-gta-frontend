import { useState } from "react";
import { Item } from "./items/item-rang";
import { List } from "./items/item-rang/style";
import bgM from "@/assets/new/images/backgrounds/main/rating_list-bgM.webp";
import bg from "@/assets/new/images/backgrounds/main/rating_list-bg.webp";
import GameNeonCard from "@/components/cards/game/neon/neon";
import { useMediaQuery } from "react-responsive";
import { top_games_settings } from "../components/sliders/settings";
import { HeadTab } from "./components/head";
import { EmptyBlock } from "./components/empty";
import { Load } from "./components/load";
import Slider from "@/components/swiper";
import { SwiperSlide } from "swiper/react";
import {
  Container,
  WrapperRating,
  Content,
  TopGame,
  Frame,
  Block,
  Title,
  Flex,
  Background,
} from "./style";
import { useTranslations } from "@/helpers/useTranslations";
import {
  useGetRatingWinsQuery,
  useGetTopMonthlyGamesMutation,
} from "@/api.v2/games";
import GameNeonCardLoader from "@/components/cards/game/neon/loading";
import { useFetchGames } from "../helpers";
import { HOME_PAGE_GAMES_LIMITS } from "../const";

const getList = (param, day, month) => {
  const renderList = (list) => {
    if (Array.isArray(list) && list.length) {
      return (
        <List>
          {list.map((item, index) => (
            <Item key={item.event_id} item={item} position={index + 1} />
          ))}
        </List>
      );
    }

    return <EmptyBlock />;
  };

  switch (param) {
    case "day":
      return renderList(day?.items);
    case "month":
      return renderList(month?.items);
    default:
      return <EmptyBlock />;
  }
};

const getGamesSort = (list = []) => {
  let data = [];
  let iterator = 2;

  for (let i = 0; i < list.length; i += iterator) {
    data.push(list.slice(i, i + iterator));
  }

  return data;
};

const getCardColor = (index) => {
  const colors = [1, 2, 3];
  const colorIndex = index % 3;
  return colors[colorIndex];
};

export const RatingList = () => {
  const [activeTab, setActiveTab] = useState("day");
  const t = useTranslations();
  const point540 = useMediaQuery({ maxWidth: 540 });
  const [getTopMonthlyGames] = useGetTopMonthlyGamesMutation();

  const { isFetching: isDayGamesLoading, data: popularDayGames } =
    useGetRatingWinsQuery({
      limit: HOME_PAGE_GAMES_LIMITS.TOP_WINS_DAY,
      period: 1,
    });
  const { isFetching: isMonthGamesLoading, data: popularMonthGames } =
    useGetRatingWinsQuery({
      limit: HOME_PAGE_GAMES_LIMITS.TOP_WINS_MONTH,
      period: 30,
    });

  const { renderedItems: popularGamesItems, isLoading: isPopularGamesLoading } =
    useFetchGames("top-monthly-games", getTopMonthlyGames, {
      limit: HOME_PAGE_GAMES_LIMITS.TOP_MONTHLY_GAMES,
      period: 30,
    });

  const getGameId = (id, data) => id + (data.id || data.game_key);

  return (
    <WrapperRating>
      <Background src={point540 ? bgM : bg} loading="lazy" alt="" />
      <Frame>
        <Block>
          <Title>{t("main_page_top_wins")}</Title>
          <Container>
            <Content>
              {isDayGamesLoading || isMonthGamesLoading ? (
                <Load />
              ) : (
                <>
                  <HeadTab activeTab={activeTab} setActiveTab={setActiveTab} />
                  {getList(activeTab, popularDayGames, popularMonthGames)}
                </>
              )}
            </Content>
          </Container>
        </Block>
        {isPopularGamesLoading ? (
          <Block>
            <TopGame>
              <Slider
                settings={top_games_settings}
                title={"main_page_top_monthly_games"}
                withTextLoading
                alwaysShowArrows
              >
                {[...Array(point540 ? 2 : 3)].map((_, id) => (
                  <SwiperSlide key={id}>
                    <Flex>
                      {[...Array(2)].map((_, id) => (
                        <GameNeonCardLoader key={id} halfHeight />
                      ))}
                    </Flex>
                  </SwiperSlide>
                ))}
              </Slider>
            </TopGame>
          </Block>
        ) : (
          <Block>
            <TopGame>
              <Slider
                minLength={popularGamesItems.length > (point540 ? 4 : 6)}
                title={"main_page_top_monthly_games"}
                settings={top_games_settings}
                alwaysShowArrows
                withTextLoading
                haveData={popularGamesItems?.length}
              >
                {getGamesSort(popularGamesItems).map((item, id) => (
                  <SwiperSlide key={id}>
                    <Flex id={getCardColor(id)}>
                      {item.map((data) => (
                        <GameNeonCard
                          key={getGameId(id, data)}
                          item={data}
                          halfHeight
                          favorite
                        />
                      ))}
                    </Flex>
                  </SwiperSlide>
                ))}
              </Slider>
            </TopGame>
          </Block>
        )}
      </Frame>
    </WrapperRating>
  );
};
