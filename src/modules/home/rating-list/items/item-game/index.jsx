import React from "react";
import { useNavigate } from "react-router";
import { Avatar, Price, Text, Wrapper } from "./style";
import Broken from "@/assets/new/images/cards/game_empty.webp";
import { getImage } from "@/helpers/getImage";
import { useCommon, useUser } from "@/store/selectors";
import { LINK_TEMPLATES } from "@/const/link-templates";
import { useSnackbar } from "notistack";
import { useTranslations } from "@/helpers/useTranslations";
import { useGamePlay } from "@/helpers";

export const ItemGame = ({ item, providers, close }) => {
  const push = useNavigate();
  const { isAuth, user } = useUser();
  const { language } = useCommon();
  const { enqueueSnackbar } = useSnackbar();
  const t = useTranslations();
  const { onPlayDemo, onPlayRealShort } = useGamePlay(item);

  return (
    <Wrapper
      className="play-container"
      onClick={() => {
        if (!providers) {
          if (user?.currency && isAuth) {
            if (user.bonus_balance && !item.is_bonus_game) {
              enqueueSnackbar(t("bonus_balance_not_allowed"), {
                variant: "info",
              });
            } else {
              onPlayRealShort();
              close && close();
              window.scrollTo(0, 0);
            }
          } else {
            onPlayDemo();
            close();
          }
        } else {
          push(LINK_TEMPLATES.GAME_LISTING({ language, brand: item?.name }));
          close();
          window.scrollTo(0, 0);
        }
      }}
    >
      <Avatar className="avatar">
        <img
          src={
            getImage(item?.game_image_url) || getImage(item?.image) || Broken
          }
          alt={"game avatar"}
          onError={({ currentTarget }) => {
            currentTarget.onerror = null;
            currentTarget.src = Broken;
          }}
        />
      </Avatar>
      <Text>
        <b>{item?.game_name || item?.name}</b>
        {item?.brand_name && (
          <>
            <span className="description">{item?.brand_name}</span>
            <span className="amount">{item?.brand_name} games</span>
          </>
        )}
        {item?.provider_name && <span>{item?.provider_name}</span>}
        {item?.game_balance && (
          <Price className="price">
            <span>{item?.game_balance}</span>
            {user?.currency}
          </Price>
        )}
      </Text>
    </Wrapper>
  );
};
