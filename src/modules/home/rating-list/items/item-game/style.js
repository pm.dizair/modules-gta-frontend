import styled from "styled-components";
import { overflowText } from "@/assets/css/global";

export const Avatar = styled.div`
  width: 80px;
  min-width: 60px;
  height: 56px;
  img {
    border-radius: 8px;
    width: 100%;
    height: 100%;
    object-fit: cover;
  }
`;

export const Text = styled.div`
  display: flex;
  position: relative;
  align-items: flex-start;
  flex-direction: column;
  color: #fff;
  width: calc(100% - 106px);
  b {
    display: block;
    font-size: 18px;
    ${overflowText}
  }
  & > span {
    font-weight: 400;
    color: #a3a6b6;
    font-size: 16px;
    ${overflowText}
  }
`;

export const Price = styled.div`
  margin-top: 5px;
  font-size: 18px;
  display: flex;
  span {
    margin-right: 5px;
    display: block;
    ${overflowText}
    max-width: 150px;
  }
`;

export const Wrapper = styled.div`
  display: flex;
  width: 100%;
  align-items: center;
  justify-content: flex-start;
  gap: 8px;
  padding: 12px 16px;
  cursor: pointer;
  transition: all 0.2 linear;
`;
