import React from "react";
import { useSnackbar } from "notistack";
import Broken from "@/assets/new/images/cards/game_empty.webp";
import { useUser } from "@/store/selectors";
import { useTranslations } from "@/helpers/useTranslations";
import { useGamePlay } from "@/helpers";
import { Wrapper, Coin, Content, Name, Price, Avatar } from "./style";
import { getImage } from "@/helpers/getImage";

export const Item = ({ item, position }) => {
  const state = useUser();
  const { enqueueSnackbar } = useSnackbar();
  const { onPlayRealShort, onPlayDemo } = useGamePlay(item);
  const t = useTranslations();

  const onClick = () => {
    if (!item?.is_bonus_game && state.user.bonus_balance) {
      enqueueSnackbar(t("bonus_balance_not_allowed"), {
        variant: "info",
      });
    } else {
      if (state?.isAuth) {
        onPlayRealShort();
      } else {
        onPlayDemo();
      }
    }
  };

  return (
    <Wrapper>
      <Content>
        <div>
          <Coin rang={position}>{position}</Coin>
        </div>
        <Avatar onClick={onClick}>
          <img
            src={getImage(item?.game_image_url) || Broken}
            alt=""
            onError={({ currentTarget }) => {
              currentTarget.onerror = null; // prevents looping
              currentTarget.src = Broken;
            }}
          />
        </Avatar>
        <Name>
          <b onClick={onClick}>{item?.game_name} </b>
          <span>
            <i>in</i> {item?.brand_name} <i>by</i> {item?.user_name}
          </span>
        </Name>
      </Content>
      <Price>
        <span>{Math.round(item?.game_balance * 10) / 10}</span>
        {state.isAuth ? state.user?.currency : "EUR"}
      </Price>
    </Wrapper>
  );
};
