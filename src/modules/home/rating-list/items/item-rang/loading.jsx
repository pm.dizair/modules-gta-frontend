import NeonSkeleton from "@/components/skeleton/neon";
import React from "react";
import { Wrapper, Coin, Content, Name, Price, Avatar } from "./style";

export const ItemLoader = () => {
  return (
    <Wrapper>
      <Content style={{ width: "100%" }}>
        <div>
          <Coin>
            <NeonSkeleton width={100} height={100} />
          </Coin>
        </div>
        <Avatar>
          <NeonSkeleton width={100} height={100} />
        </Avatar>
        <Name>
          <b>
            <NeonSkeleton />
          </b>
          <span>
            <NeonSkeleton />
          </span>
        </Name>
      </Content>
      <Price>
        <NeonSkeleton height={"100%"} width={50} />
      </Price>
    </Wrapper>
  );
};
