import styled from "styled-components";
import { overflowText } from "@/assets/css/global";

export const List = styled.div`
  width: 100%;
  padding: 0 40px;
  margin-top: 15px;
  
  @media screen and (max-width: 1440px) {
    padding: 0 36px;
    margin-top: 10px;
  }
  @media screen and (max-width: 767px) {
    padding: 0 16px;
  }
`;

export const Wrapper = styled.div`
  padding: 18px 0;
  display: flex;
  width: 100%;
  justify-content: space-between;
  align-items: center;
  position: relative;
  cursor: pointer;
  &::after {
    content: "";
    bottom: -1px;
    left: 0;
    position: absolute;
    width: 100%;
    background: radial-gradient(
      50% 50% at 50% 50%,
      #eff1ff 0%,
      rgba(218, 237, 255, 0) 100%
    );
    filter: drop-shadow(0px 0px 20px #5ad7ff);
    height: 2px;
  }
  &:last-child {
    &::after {
      display: none;
    }
  }
  transition: all 0.4s linear;
  @media screen and (min-width: 1024px) {
    &:hover {
      background: rgba(255, 255, 255, 0.01);
      &::after {
        height: 3px;
      }
    }
  }
  @media screen and (max-width: 1440px) {
    padding: 12px 0;
  }
`;
export const Content = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  width: calc(100% - 140px);
  @media screen and (max-width: 1440px) {
    width: calc(100% - 80px);
  }
`;
export const Avatar = styled.div`
  overflow: hidden;
  height: 74px;
  width: 92px;
  border-radius: 4px;
  img {
    width: 100%;
    height: 100%;
  }
  @media screen and (max-width: 1800px) {
    height: 56px;
    width: 72px;
  }
  @media screen and (max-width: 540px) {
    height: 40px;
    width: 52px;
  }
`;

export const Coin = styled.div`
  width: 40px;
  height: 40px;
  display: flex;
  overflow: hidden;
  justify-content: center;
  align-items: center;
  text-align: center;
  border-radius: 50%;
  margin-right: 40px;
  @media screen and (max-width: 1440px) {
    width: 32px;
    margin-right: 24px;
    height: 32px;
    font-size: 14px;
  }
  @media screen and (max-width: 767px) {
    margin-right: 16px;
  }
  @media screen and (max-width: 540px) {
    width: 24px;
    font-size: 12px;
    margin-right: 6px;
    height: 24px;
  }
  ${(props) =>
    props.rang === 1
      ? `
        background-color: #DCB03F;
        box-shadow: 0px 0px 40px rgba(235, 218, 128, 0.22);
    `
      : props.rang === 2
      ? `
        background: #BCBDBD;
        box-shadow: 0px 0px 40px rgba(255, 255, 255, 0.4);
      `
      : props.rang === 3
      ? `
      background: #D07810;
        box-shadow: 0px 0px 40px rgba(244, 135, 34, 0.43);
      `
      : `
        background-color: #cfe8ff;
        box-shadow: 0px 0px 60px rgba(34, 168, 244, 0.8);
      `}
`;
export const Name = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: flex-start;
  flex-direction: column;
  margin-left: 20px;
  width: 100%;
  max-width: calc(100% - 210px);
  color: white;
  b {
    font-size: 18px;
    ${overflowText}
    @media screen and (max-width: 540px) {
      font-size: 16px;
    }
  }
  span {
    color: #a3a6b6;
    font-size: 14px;
    ${overflowText}
    i {
      font-style: normal;
      color: #ffffff;
    }
    @media screen and (max-width: 540px) {
      font-size: 14px;
    }
  }
  @media screen and (max-width: 1800px) {
    b {
      font-size: 14px;
    }
  }
  @media screen and (max-width: 1440px) {
    max-width: calc(100% - 160px);
  }
  @media screen and (max-width: 540px) {
    max-width: calc(100% - 100px);
    margin-left: 8px;
  }
`;
export const Price = styled.b`
  font-size: 18px;
  white-space: nowrap;
  display: flex;
  color: #fff;
  font-weight: 600;
  span {
    margin-right: 5px;
    display: block;
    ${overflowText}
    max-width: 90px;
  }
  @media screen and (max-width: 1440px) {
    font-size: 14px;
    span {
      max-width: 60px;
    }
  }
  @media screen and (max-width: 1440px) {
    font-size: 12px;
    span {
      max-width: 50px;
    }
  }
`;
