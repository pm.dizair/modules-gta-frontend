import styled from "styled-components";
import { Wrapper } from "@/components/containers/container";
import { GTA_TEXT } from "@/assets/css/global";

export const Frame = styled.div`
  display: flex;
  width: 100%;
  align-items: flex-start;
  justify-content: space-between;
  max-width: 1920px;
  margin: auto;
  position: relative;
  overflow: hidden;
  z-index: 2;

  padding: 40px 0px 80px;

  @media screen and (max-width: 1024px) {
    padding: 0px 0px 100px;
    flex-direction: column;
    gap: 56px;
  }
  @media screen and (max-width: 767px) {
    padding: 0px 0px 60px 0px;
  }
  @media screen and (max-width: 540px) {
    padding: 30px 0px 10px 0px;
    gap: 20px;
  }
`;
export const Container = styled.div`
  position: relative;
  width: 100%;
  min-height: 654px;
  border-radius: 4px;

  &::after,
  &::before {
    content: "";
    z-index: 1 !important;
    position: absolute;
    inset: -2px;
    background-color: #fff;
  }
  &::after {
    border-radius: 4px;
  }
  &::before {
    filter: blur(4px);
    transform: translate3d(0, 0, 0);
  }
  &::after,
  &::before {
    border-radius: 4px;
    transition: --out 0.5s;
    background: linear-gradient(
      -45deg,
      rgba(255, 244, 215, 0.8) 0%,
      rgba(255, 244, 215, 0.6) 5%,
      rgba(255, 244, 215, 0) 50%,
      rgba(255, 244, 215, 0.6) 95%,
      rgba(255, 244, 215, 0.8) 100%
    );
  }
  @media screen and (max-width: 1800px) {
    min-height: 598px;
  }
  @media screen and (max-width: 1600px) {
    min-height: 548px;
  }
  @media screen and (max-width: 1440px) {
    min-height: 490px;
  }
  @media screen and (max-width: 1280px) {
    min-height: 470px;
  }
  @media screen and (max-width: 540px) {
    min-height: 440px;
  }
`;
export const Title = styled.h2`
  height: 56px;
  font-weight: 700;
  font-size: 32px;
  line-height: 32px;
  position: relative;
  z-index: 4;
  color: #fff;
  margin-bottom: 24px;

  display: flex;
  align-items: center;

  @media screen and (max-width: 1440px) {
    font-size: 32px;
  }
  @media screen and (max-width: 767px) {
    width: 100%;
    text-align: center;
  }
  @media screen and (max-width: 540px) {
    font-size: 24px;
  }
  @media screen and (max-width: 360px) {
    font-size: 18px !important;
    margin-bottom: 10px;
  }
`;
export const Block = styled.div`
  position: relative;

  &:first-child {
    width: 40%;
    padding-left: 80px;
  }
  &:last-child {
    width: 50%;
    height: 100%;
  }
  .swiper
    .swiper-initialized
    .swiper-horizontal
    .swiper-backface-hidden
    .slider_container {
    height: 100%;
  }

  .arrow_prev,
  .arrow_next {
    border: 1px solid rgba(255, 255, 255, 0.5);
    svg {
      path {
        stroke: #fff;
      }
    }
  }
  ${(props) =>
    props.load &&
    `

  `}

  h1 {
    font-weight: 700;
    font-size: 36px;
    position: relative;
    z-index: 4;
    color: #fff;
    line-height: 48px;
    @media screen and (max-width: 1440px) {
      font-size: 32px;

      &:first-child {
        padding-left: 64px;
      }
    }
    @media screen and (max-width: 540px) {
      font-size: 24px;
    }
    @media screen and (max-width: 360px) {
      font-size: 18px;
      margin-bottom: 5px;
    }
  }
  @media screen and (max-width: 1280px) {
    &:first-child {
      width: 45%;
      padding-left: 64px;
    }
  }

  @media screen and (max-width: 1024px) {
    .arrow_prev,
    .arrow_next {
      background: var(--grey_dark);
    }

    &:first-child,
    &:last-child {
      width: 100%;
      padding-left: 32px;
    }
    &:first-child {
      padding-right: 32px;
      h2 {
        text-align: center;
        margin-top: 50px;
      }
    }
  }
  @media screen and (max-width: 768px) {
    &:first-child,
    &:last-child {
      padding-left: 16px;
    }
    &:first-child {
      padding-right: 16px;
    }
  }
  @media screen and (max-width: 540px) {
    &:first-child {
      h2 {
        margin-top: 10px;
      }
    }
  }
`;
export const LoadBlock = styled(Block)``;
export const Content = styled.div`
  position: absolute;
  inset: 0;
  height: 100%;
  width: 100%;
  border-radius: 4px;
  z-index: 2;
  background: var(--grey_dark);
  overflow: hidden;
`;

export const Switcher = styled.div`
  display: flex;
  gap: 48px;
`;
export const Tab = styled.button`
  font-size: 24px;
  color: ${(props) => (props.isActive ? `#000` : `#A3A6B6`)};
  margin-bottom: 30px;
`;
export const WrapperRating = styled(Wrapper)`
  display: flex;
  padding: 0 !important;
  max-width: 100vw;
  justify-content: space-between;
  align-items: center;
  position: relative;
  z-index: 2;
  transform: translateY(50px);
  margin: 0px auto 28px auto;

  .slider_container {
    overflow: hidden;
    margin-left: 0px !important;
  }

  .swiper_wrap .top-panel {
    padding-right: 80px;
  }
  .slider_container {
    margin-right: 80px;
  }

  @media screen and (max-width: 1440px) {
    .swiper_wrap .top-panel {
      padding-right: 64px;
    }
    .slider_container {
      margin-right: 64px;
    }
  }

  @media screen and (max-width: 1024px) {
    .swiper_wrap .top-panel {
      padding-right: 32px;
    }
    .slider_container {
      margin-right: 32px;
    }
  }

  @media screen and (max-width: 767px) {
    margin: 0 auto;
    .swiper_wrap .top-panel {
      padding-right: 16px;
    }
    .slider_container {
      margin-right: 16px;
    }
  }
`;
export const Background = styled.img`
  position: absolute;
  top: -40px;
  bottom: -40px;
  left: 50%;
  transform: translate(-50%, 0);
  width: auto;
  height: calc(100% + 100px);
  object-position: center;
  object-fit: contain;
  @media screen and (max-width: 767px) {
    object-fit: cover;
    width: 100%;
  }
`;
export const Head = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  padding-top: 28px;
  gap: 48px;
  @media screen and (max-width: 1440px) {
    padding-top: 24px;
  }
`;
export const Period = styled.button`
  font-weight: 700;
  font-size: 24px;
  line-height: 32px;
  color: rgba(163, 166, 182, 0.6);
  transition: all 0.3s linear;
  ${(props) =>
    props.active &&
    `
    color: #FFFFFF; 
  `}
  @media screen and (max-width: 1440px) {
    font-size: 18px;
    line-height: 24px;
  }
`;
export const TopGame = styled.div`
  width: 100%;
  height: 100%;
  .swiper-wrapper {
    height: 654px !important;
  }
  .swiper-slide {
    height: 100%;
  }
  .card_game-aspect {
    height: 100%;
  }
  z-index: 2;
  .top-panel {
    padding: 0 80px 0 0;
    margin-bottom: 16px;
    height: 64px;
  }

  @media screen and (max-width: 1800px) {
    .swiper-wrapper {
      height: 600px !important;
    }
  }
  @media screen and (max-width: 1600px) {
    .swiper-wrapper {
      height: 548px !important;
    }
  }

  @media screen and (max-width: 1440px) {
    .swiper-wrapper {
      height: 490px !important;
    }
    .top-panel {
      padding: 0 64px 0 0;
    }
  }
  @media screen and (max-width: 1280px) {
    .swiper-wrapper {
      height: 470px !important;
    }
  }
  @media screen and (max-width: 1024px) {
    .swiper-wrapper {
      height: auto !important;
    }
    .card_game-aspect {
      height: auto;
    }
    .top-panel {
      padding: 0 32px 0 0;
    }
  }
  @media screen and (max-width: 767px) {
    .top-panel {
      padding: 0 16px 0 0;
      height: 48px;
    }
  }
  @media screen and (max-width: 540px) {
    .swiper-wrapper {
      height: 100vw !important;
    }
    .card_game-aspect {
      height: 100%;
    }
    margin-top: 24px;
  }
  @media screen and (max-width: 360px) {
    .top-panel {
      margin: 10px 0;
      h2 {
        font-size: 18px;
      }
    }
  }
`;

export const Empty = styled.div`
  padding: 20px 90px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  gap: 20px;
  img {
    width: 100%;
    height: 100%;
    object-position: bottom;
    object-fit: contain;
    max-width: 100%;
    position: absolute;
    max-height: 400px;
    bottom: 0;
    z-index: -1;
  }
  @media screen and (max-width: 1440px) {
    padding: 42px 60px;
    img {
      max-height: 340px;
    }
  }
  @media screen and (max-width: 1280px) {
    padding: 36px 30px;
  }
  @media screen and (max-width: 540px) {
    padding: 24px;
    .color-btn {
      padding: 8px 12px;
      font-size: 12px;
    }
  }
`;
export const EmptyTitle = styled.h2`
  font-weight: 400;
  font-size: 28px;
  line-height: 30px;
  text-align: center;
  ${GTA_TEXT}
  margin: 0 !important;
  @media screen and (max-width: 540px) {
    font-size: 24px;
    line-height: 28px;
  }
`;

export const Flex = styled.div`
  height: 100%;
  display: flex;
  flex-direction: column;
  gap: 16px;
  justify-content: space-between;

  ${(props) =>
    props.id === 1 &&
    `

 & > *:first-child {
    .circleBlur {
      background: #ff3ec9;
      background: radial-gradient(
        circle,
        rgba(255, 62, 201, 1) 0%,
        rgba(255, 62, 201, 0.5424763655462185) 40%,
        rgba(9, 9, 121, 0) 90%
      );
    }
}
& > *:nth-child(2) {
   .circleBlur {
      background: #66ff58;
      background: radial-gradient(
        circle,
        rgba(102, 255, 88, 1) 0%,
        rgba(102, 255, 88, 0.5424763655462185) 40%,
        rgba(9, 9, 121, 0) 90%
      );
    }
}
`};
  ${(props) =>
    props.id === 2 &&
    `
 & > *:first-child  {
     .circleBlur {
      background: #ff5858;
      background: radial-gradient(
        circle,
        rgba(255, 88, 88, 1) 0%,
        rgba(255, 88, 88, 0.5424763655462185) 40%,
        rgba(9, 9, 121, 0) 90%
      );
    }
}
& > *:nth-child(2) {
    .circleBlur {
      background: #c071ff;
      background: radial-gradient(
        circle,
        rgba(192, 113, 255, 1) 0%,
        rgba(192, 113, 255, 0.5424763655462185) 40%,
        rgba(9, 9, 121, 0) 90%
      );
    }
}
`};
  ${(props) =>
    props.id === 3 &&
    `
 & > *:first-child  {
     .circleBlur {
      background: #ffb156;
      background: radial-gradient(
        circle,
        rgba(255, 177, 86, 1) 0%,
        rgba(255, 177, 86, 0.5424763655462185) 40%,
        rgba(9, 9, 121, 0) 90%
      );
    }
}
& > *:nth-child(2) {
    .circleBlur {
      background: #47a7ff;
      background: radial-gradient(
        circle,
        rgba(71, 167, 255, 1) 0%,
        rgba(71, 167, 255, 0.5424763655462185) 40%,
        rgba(9, 9, 121, 0) 90%
      );
    }
}
`};
`;
