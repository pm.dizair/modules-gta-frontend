import styled from "styled-components";
import { GTA_TEXT } from "@/assets/css/global";

export const Banner = styled.div`
  & > img {
    width: 100%;
    display: block;
    height: 100vh;
    max-height: 825px;
    object-fit: cover;
    object-position: bottom;
    @media screen and (max-width: 1440px) {
      max-height: 680px;
    }
    @media screen and (max-width: 1280px) {
      max-height: 620px;
    }
    @media screen and (max-width: 1024px) {
      height: fit-content;
    }
  }
`;
export const Title = styled.div`
  margin: 24px 0;
  max-width: 500px;
  display: -webkit-box;
  -webkit-line-clamp: 3;
  -webkit-box-orient: vertical;
  overflow: hidden;
  text-overflow: ellipsis;
  max-height: calc(75px * 3);
  color: #ffffff;
  font-style: normal;
  font-weight: 400;
  font-size: 62px;
  line-height: 68px;
  ${GTA_TEXT}
  z-index: 2;
  position: relative;
  @media screen and (max-width: 1440px) {
    font-size: 58px;
  }
  @media screen and (max-width: 1024px) {
    font-size: 48px;
    line-height: 52px;
    max-height: calc(58px * 3);
    margin: 12px 0;
    text-align: center;
    max-width: 100%;
  }
  @media screen and (max-width: 767px) {
    font-size: 36px;
    line-height: 40px;
  }
`;

export const SubTitle = styled.div`
  font-family: "Kaushan Script";
  font-style: normal;
  font-weight: 400;
  font-size: 24px;
  line-height: 40px;
  color: #ffffff;
  z-index: 2;
  position: relative;
  max-width: 500px;

  display: -webkit-box;
  -webkit-line-clamp: 2;
  -webkit-box-orient: vertical;
  overflow: hidden;
  text-overflow: ellipsis;

  @media screen and (max-width: 1024px) {
    text-align: center;
    font-size: 20px;
    line-height: 24px;
  }
`;
export const Text = styled.p`
  margin-bottom: 40px;
  z-index: 2;
  font-size: 20px;
  line-height: 24px;
  letter-spacing: 0.01em;
  margin-right: 10px;
  position: relative;
  max-width: 500px;

  display: -webkit-box;
  -webkit-line-clamp: 3;
  -webkit-box-orient: vertical;
  overflow: hidden;
  text-overflow: ellipsis;

  @media screen and (max-width: 1024px) {
    text-align: center;
    font-size: 18px;
    margin-right: 0;
    margin-bottom: 24px;
    line-height: 22px;
  }
  @media screen and (max-width: 540px) {
    text-align: center;
    font-size: 16px;
    margin-right: 0;
    margin-bottom: 20px;
    line-height: 20px;
  }
`;

export const Content = styled.div`
  width: 100%;
  min-height: 100vh;
  background: var(--black);
  position: relative;
  z-index: 10;
  display: flex;
  flex-direction: column;
  gap: 60px;
  @media screen and (max-width: 767px) {
    gap: 36px;
  }
`;

export const Shadow = styled.div`
  background-color: var(--black);
  box-shadow: 0px 0px 35px 40px var(--black);
  height: 10px;
  position: absolute;
  width: 100%;
  top: 0;
  left: 0;
  transform: translateY(-55%);
`;

export const WrapCurve = styled.div`
  display: flex;
  flex-direction: column;
  gap: 32px;
  overflow: hidden;
  @media screen and (max-width: 540px) {
    gap: 0px;
  }
`;
