import TournamentCard from "@/components/cards/tournament";
import TournamentCardLoader from "@/components/cards/tournament/load";
import { TournamentWrap } from "../components/sliders/style";
import { useGetTournamentsQuery } from "@/api.v2/tournament";
import { useCommon } from "@/store/selectors";
import { ACTIVE } from "@/modules/tournaments/data";

const TournamentCurrent = () => {
  const { language } = useCommon();

  const { data: tournaments, isLoading } = useGetTournamentsQuery({
    language,
    status: ACTIVE,
    main_page: true,
  });

  if (isLoading) {
    return (
      <TournamentWrap>
        <TournamentCardLoader />
      </TournamentWrap>
    );
  }

  if (!tournaments?.length) {
    return null;
  }

  return (
    <TournamentWrap>
      <TournamentCard item={tournaments[0]} />
    </TournamentWrap>
  );
};

export default TournamentCurrent;
