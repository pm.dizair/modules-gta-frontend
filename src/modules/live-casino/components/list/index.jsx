import { AutoLoadReduxModule } from "@/components/autoload";
import { NotFound } from "@/components/banner/not-found";
import { useParams } from "react-router";
import GameCardLoader from "@/components/cards/game/loading";
import { autoloadModuleSlice } from "@/store/reducers/cache";
import AutoLoadGameCard from "@/components/cards/game/autoload";

export const LiveCasinoList = ({ fetch }) => {
  const { tab } = useParams();

  const config = {
    listStyle: {
      gap: "12px 16px",
    },
    reduxSlice: autoloadModuleSlice,
    className: "virtual-list",
    fetch,
    preloadRows: 12,
    columns: {
      767: 2,
      default: 5,
    },
    rows: {
      767: 6,
      default: 5,
    },
  };

  return (
    <AutoLoadReduxModule config={config} states={{ tab: `live-casino/${tab}` }}>
      {(items) =>
        items.length ? (
          items.map(({ item, observerParams }, index) => {
            if (item) {
              return (
                <AutoLoadGameCard
                  observerParams={observerParams}
                  key={item.id}
                  item={item}
                  favorite
                />
              );
            }
            return <GameCardLoader key={-index} />;
          })
        ) : (
          <NotFound />
        )
      }
    </AutoLoadReduxModule>
  );
};
