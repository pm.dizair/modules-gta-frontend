import { overflowText } from "@/assets/css/global";
import styled from "styled-components";

export const Wrapper = styled.div`
  width: 100%;
  gap: 16px 12px;
  display: flex;
  align-items: flex-start;
  flex-wrap: wrap;
  justify-content: flex-start;
  margin-top: 12px;
  .card_game {
    border-radius: 4px;
  }
  .card_game-banner {
    border-radius: 4px;
    img {
      border-radius: 4px;
    }
  }
  .title {
    font-weight: 400;
    font-size: 13px;
    line-height: 24px;
    letter-spacing: 0.01em;
    color: #ffffff;
    ${overflowText}
  }
  .sub {
    font-weight: 400 !important;
    font-size: 14px !important;
    line-height: 18px !important;
    color: #a3a6b6;
    ${overflowText}
  }
  & > div {
    width: calc(100% / 6 - 10px);
    @media screen and (max-width: 1440px) {
      width: calc(100% / 4 - 9px);
    }
    @media screen and (max-width: 1280px) {
      width: calc(100% / 3 - 8px);
    }
    @media screen and (max-width: 767px) {
      width: calc(100% / 2 - 6px);
    }
    @media screen and (max-width: 360px) {
      width: 100%;
    }
  }
  ${(props) => props.hidden && "display: none"}
`;
