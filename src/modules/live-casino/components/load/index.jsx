import { GameBannerLoader } from "@/components/banner/game-banner/loading";
import React from "react";
import { useMediaQuery } from "react-responsive";
import { Filter, Title } from "../../style";
import NeonSkeleton from "@/components/skeleton/neon";

export const Load = () => {
  const matches767 = useMediaQuery({ maxWidth: 767 });

  return (
    <>
      {!matches767 && <GameBannerLoader />}
      <Filter>
        <Title>
          <NeonSkeleton />
        </Title>
      </Filter>
    </>
  );
};
