import { IconSearch } from "@/components/icons";
import { useTranslations } from "@/helpers/useTranslations";
import React from "react";
import { Button, Input, Wrapper } from "./style";

export const Search = ({ setQuery }) => {
  const t = useTranslations();
  return (
    <Wrapper>
      <Button>
        <IconSearch width="20" fill="var(--gold)" />
      </Button>
      <Input
        onChange={(e) => setQuery(e.target.value)}
        placeholder={t("search_games")}
      />
    </Wrapper>
  );
};
