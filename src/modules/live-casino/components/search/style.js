import styled from "styled-components";

export const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: calc(100% / 6 * 2 - 8px);
  padding: 8px 16px;
  background: #ffffff;
  border-bottom: 1px solid #dddddd;
  height: 48px;
  gap: 16px;
  @media screen and (max-width: 767px) {
    width: 100%;
    margin-bottom: 10px;
  }
`;
export const Input = styled.input`
  all: unset;
  cursor: text;
  width: 100%;
  font-family: "Outfit";
  font-style: normal;
  font-weight: 400;
  font-size: 16px;
  line-height: 22px;
  &::placeholder {
    color: var(--grey_light);
  }
`;

export const Button = styled.button`
  display: flex;
  justify-content: center;
  align-items: center;
  svg {
    width: 20px;
    height: 20px;
  }
  @media screen and (max-width: 767px) {
    width: 20px;
    margin: 0 10px;
  }
  @media screen and (max-width: 540px) {
    margin: 0px;
  }
`;
