import { Container } from "@/components/containers";
import React, { useEffect, useState } from "react";
import { Filter, Title, Wrapper } from "./style";
import { LiveCasinoList } from "./components/list";
import { useMediaQuery } from "react-responsive";
import { useNavigate, useParams } from "react-router";
import { GameBanner } from "@/components/banner/game-banner";
import { Load } from "./components/load";
import { useTranslations } from "@/helpers/useTranslations";
import { useCommon, useUser } from "@/store/selectors";
import { useGetPromotionRandomMutation } from "@/api.v2/promotion";
import { AllGamesWrapper, Background } from "../games/style";
import { useGetGamesMutation } from "@/api.v2/games";
import { OutlineViolent } from "@/components/ui/buttons/violent-btn/outline";
import { LINK_TEMPLATES } from "@/const/link-templates";

export const LiveCasino = () => {
  const [imgLoaded, setImgLoaded] = useState(false);
  const [getPromotionRandom, { data: promotion, isLoading, isUninitialized }] =
    useGetPromotionRandomMutation();

  const matches767 = useMediaQuery({ maxWidth: 767 });
  const { language, hasPromotions } = useCommon();
  const { tab } = useParams();
  const { user } = useUser();

  const t = useTranslations();
  const push = useNavigate();

  const [getGames] = useGetGamesMutation();

  const redirectToDetail = () => {
    push(`/${language}/promotions/detail/${promotion.id}`);
    window.scrollTo(0, 0);
  };

  const fetchLiveCasino = async ({ page, limit }) => {
    const body = {
      category: "Live casino",
      limit,
      page,
    };

    const res = await getGames(body);

    return res.data;
  };

  const fetchLiveCasinoShort = async ({ limit }) => {
    const body = {
      category: "Live casino",
      page: 1,
      limit,
    };

    const res = await getGames(body);

    return {
      items: res.data.items,
      total: res.data.items.length,
    };
  };

  const showImage = () => {
    setImgLoaded(true);
  };

  useEffect(() => {
    if (hasPromotions) {
      getPromotionRandom({ language, userId: user?.id });
    }
  }, [hasPromotions, language, user?.id]);

  return (
    <Background img={null} imgLoaded={imgLoaded} onLoad={showImage}>
      <Container>
        <Wrapper>
          {hasPromotions ? (
            isLoading || isUninitialized ? (
              <Load />
            ) : (
              <>
                {!matches767 && promotion?.id && (
                  <GameBanner item={promotion} onClick={redirectToDetail} />
                )}
                <Filter>
                  <Title>{t("live_casino_page_header")}</Title>
                </Filter>
              </>
            )
          ) : null}

          {tab === "all" && <LiveCasinoList fetch={fetchLiveCasino} />}
          {tab !== "all" && (
            <>
              <LiveCasinoList fetch={fetchLiveCasinoShort} />
              <AllGamesWrapper>
                {isLoading || isUninitialized ? (
                  <></>
                ) : (
                  <OutlineViolent
                    onClick={() => {
                      push(
                        LINK_TEMPLATES.LIVE_CASINO_LISTING({
                          tab: "all",
                          language,
                        })
                      );
                    }}
                  >
                    {t("live_casino_page_all_live_games_button")}
                  </OutlineViolent>
                )}
              </AllGamesWrapper>
            </>
          )}
        </Wrapper>
      </Container>
    </Background>
  );
};
