import styled from "styled-components";

export const Wrapper = styled.div`
  padding: 100px 0 60px 0;
  .banner {
    margin: 0 0 20px 0;
  }
  &.title {
    font-size: 26px;
    line-height: 30px;
  }
  &.text {
    font-size: 54px;
    line-height: 66px;
  }
  button {
    white-space: nowrap;
  }
  @media screen and (max-width: 767px) {
    padding: 76px 0 32px 0;
  }
`;
export const Title = styled.h1`
  white-space: nowrap;
  width: 100%;
`;
export const Filter = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin: 32px 0 24px 0;
  @media screen and (max-width: 767px) {
    flex-direction: column;
    align-items: flex-start;
    gap: 20px;
    margin: 4px 0 16px 0;
  }
`;
