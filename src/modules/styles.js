import dib from "@/assets/icons/dib.svg";
import styled from "styled-components";

export const Description = styled.div`
  max-width: 800px;
  margin: auto;
  a {
    color: #fff;
    text-decoration: underline;
    font-size: 14px;
    font-weight: 400;
    line-height: 20px;
  }
  h3 {
    color: #fff;
  }
  p {
    color: #c6c7ce;
    font-size: 14px;
    font-weight: 400;
    line-height: 20px;
    margin: 16px 0;
  }
  strong {
    color: #c6c7ce;
    font-weight: 500;
  }

  li {
    margin: 8px 5px 8px 0;
  }
  ol {
    padding: 0 16px;
    list-style-type: decimal;
    & > li {
      font-size: 14px;
      line-height: 20px;
      list-style-type: decimal;
      display: list-item;
    }
  }
  li {
    font-weight: 200;
    font-size: 14px;
    position: relative;
    line-height: 20px;
    color: #c6c7ce;
  }

  ul {
    padding: 0 0 0 16px;

    li {
      position: relative;
      &::before {
        position: absolute;
        left: 0;
        top: 0;
        transform: translate(-100%, 0%);
        content: url(${dib});
        display: block;
        padding: 2px 6px 0px 6px;
      }
    }
  }
  @media screen and (max-width: 767px) {
    ol {
      padding: 0 2px;
    }
  }
`;
