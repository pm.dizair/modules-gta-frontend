import ForgotPassword from "@/modules/forgot-password";
import Home from "@/modules/home";
import PaymentAccepted from "@/modules/payment-accepted";
import PaymentHistory from "@/modules/payment-history";
import Profile from "@/modules/profile";
import { createContext, useEffect, useState } from "react";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import PageLoader from "./components/loader/page-loader";
import { useListenSockets } from "./helpers/sockets/useListenSockets";
import { useInitFetch } from "./helpers/useInitFetch";
import { useTranslations } from "./helpers/useTranslations";
import Layout from "./layout";
import { Page404 } from "./modules/empty-pages/error/404";
import { GameDetailPage } from "./modules/game-detail";
import { GamesPage } from "./modules/games";
import { LiveCasino } from "./modules/live-casino";
import MailVerirfication from "./modules/mail-verification";
import PaymentCanceled from "./modules/payment-canceled";
import { PromotionDetailPage } from "./modules/promotion_detail";
import { PromotionPage } from "./modules/promotions";
import { ShopPage } from "./modules/shop";
import { TournamentDetails, TournamentList } from "./modules/tournaments";
import { VipPage } from "./modules/vip";
import { WalletPage } from "./modules/wallet";
import { WhitePage } from "./modules/whitepaper";
import { useCommon, useUser } from "./store/selectors";
import ErrorPage from "./modules/empty-pages/error";
import { useCheckProfileStatusQuery } from "./api/user";
import PaymentPending from "./modules/payment-pending";

export const CustomRouterContext = createContext();

const publicRouter = [
  {
    path: "/:lang/",
    element: <Home />,
  },
  {
    path: "/:lang/games/:category/:provider/:hashtag",
    element: <GamesPage />,
  },
  {
    path: "/:lang/games/:category/:provider",
    element: <GamesPage />,
  },
  {
    path: "/:lang/shop",
    element: <ShopPage />,
  },
  {
    path: "/:lang/shop/:tab",
    element: <ShopPage />,
  },
  {
    path: "/:lang/shop/:tab/:page",
    element: <ShopPage />,
  },

  {
    path: "/:lang/wp/:tab",
    element: <WhitePage />,
  },
  {
    path: "/:lang/live-casino/:tab",
    element: <LiveCasino />,
  },
  {
    path: "/:lang/promotions/:page",
    element: <PromotionPage />,
  },
  {
    path: "/:lang/promotions/detail/:id",
    element: <PromotionDetailPage />,
  },
  {
    path: "/:lang/payment-accepted/:id/*",
    element: <PaymentAccepted />,
  },
  {
    path: "/:lang/payment-accepted/*",
    element: <PaymentAccepted />,
  },
  {
    path: "/:lang/forgot-password/:id",
    element: <ForgotPassword />,
  },
  {
    path: "/:lang/vip_level",
    element: <VipPage />,
  },
  {
    path: "/:lang/tournaments",
    element: <TournamentList />,
  },
  {
    path: "/:lang/tournament/:id",
    element: <TournamentDetails />,
  },
  {
    path: "/:lang/mail-verification/:id",
    element: <MailVerirfication />,
  },
];

const authRouter = [
  {
    path: "/:lang/wallet",
    element: <WalletPage />,
  },
  {
    path: "/:lang/wallet/:tab",
    element: <WalletPage />,
  },
  {
    path: "/:lang/wallet/:tab/:page",
    element: <WalletPage />,
  },

  {
    path: "/:lang/profile",
    element: <Profile />,
  },
  {
    path: "/:lang/profile/:tab",
    element: <Profile />,
  },
  {
    path: "/:lang/profile/:tab/:page",
    element: <Profile />,
  },
  {
    path: "/:lang/forgot-password/:id",
    element: <ForgotPassword />,
  },

  {
    path: "/:lang/payment-canceled/:id/*",
    element: <PaymentCanceled />,
  },
  {
    path: "/:lang/payment-history/",
    element: <PaymentHistory />,
  },
  {
    path: "/:lang/payment-pending/:id/*",
    element: <PaymentPending />,
  },
  {
    path: "/:lang/payment-pending/*",
    element: <PaymentPending />,
  },
  {
    path: "/:lang/payment-pending",
    element: <PaymentPending />,
  },
];

const RouterChild = () => {
  const { isAuth } = useUser();
  const routerPath = isAuth ? [...publicRouter, ...authRouter] : publicRouter;

  const router = createBrowserRouter([
    {
      path: "/:lang/play/:id/:mode",
      element: <GameDetailPage />,
    },
    {
      path: "/:lang/play/:id/:mode/:fullscreen",
      element: <GameDetailPage />,
    },
    {
      path: "/",
      element: <Layout />,
      errorElement: <ErrorPage />,
      children: routerPath,
    },
    {
      path: "*",
      element: <Page404 />,
    },
  ]);

  return <RouterProvider router={router} />;
};

const Router = () => {
  const { isLoaded } = useUser();
  const { isLoading, translations, language } = useCommon();
  const t = useTranslations();

  useEffect(() => {
    if (translations) {
      document.title = t("site_title");
    }
  }, [translations, language]);

  useInitFetch();
  useListenSockets();

  if (!isLoaded || isLoading) {
    return <PageLoader />;
  }

  return <RouterChild />;
};

const CheckUserStatusWrapper = () => {
  const [isLoaded, setIsLoaded] = useState(false);
  const [isProfileStatusFalse, setIsProfileStatusFalse] = useState(false);

  const { isFetching, isError, error } = useCheckProfileStatusQuery();

  useEffect(() => {
    if (!isFetching) {
      if (isError && error.status === 403) {
        localStorage.clear();
        setIsProfileStatusFalse(true);
      }
      setIsLoaded(true);
    }
  }, [isFetching]);

  if (isFetching || !isLoaded) {
    return <PageLoader />;
  }

  return (
    <CustomRouterContext.Provider value={{ isProfileStatusFalse }}>
      <Router />
    </CustomRouterContext.Provider>
  );
};

export default CheckUserStatusWrapper;
